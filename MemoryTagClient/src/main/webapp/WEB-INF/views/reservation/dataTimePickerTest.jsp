<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

<link rel="stylesheet" href="//cdn.rawgit.com/fgelinas/timepicker/master/jquery.ui.timepicker.css">
<script src='//cdn.rawgit.com/fgelinas/timepicker/master/jquery.ui.timepicker.js'></script>

<style>
.ui-timepicker {  width: 17em; padding: .2em .2em 0; }
.ui-timepicker-hour-cell { width: 20px;}
.ui-timepicker-hour-cell > .ui-state-default { width: 50px;}

</style>  
<script>
$(function() {
  $( "#datepicker1" ).datepicker({
    dateFormat: 'yymmdd',
    prevText: '이전 달',
    nextText: '다음 달',
    monthNames: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
    monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
    dayNames: ['일','월','화','수','목','금','토'],
    dayNamesShort: ['일','월','화','수','목','금','토'],
    dayNamesMin: ['일','월','화','수','목','금','토'],
    showMonthAfterYear: true,
    changeMonth: true,
    changeYear: true,
    yearSuffix: '년',
    
  });
});

$(function() {
    $('.timepicker').timepicker({
    	 showMinutes: false,
    });
});
</script>
 <div class="container">   
 <input type="text" id="datepicker1">

</div>
<div class="container">
<input type="text" class='timepicker'>
</div>
<button class="btn btn-success" type="button" id="reservationBtn">예약</button>
<script>
$("#reservationBtn").on("click",function(){
	
var formData = $( "#datepicker1" ).val();

var time = $(".timepicker").val();
console.log(formData);
console.log(time);
});

</script>
<%@include file="../include/footer.jsp"%>