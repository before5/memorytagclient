<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>

  <link href='https://fonts.googleapis.com/css?family=Inconsolata:400,700' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css">
  <link rel="stylesheet" href="/resources/bootstrap/multicolor/css/palette-color-picker.css">
  <style>
  
   
    
    #unique-id {
      vertical-align: top;
      font-family: 'Inconsolata', Courier, monospace;
      font-weight: 700;
      padding: 4px;
      height: 30px;
      margin-right: 8px;
    }

  </style>

  <script src="//ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.4.min.js"></script>
  <script src="/resources/bootstrap/multicolor/js/palette-color-picker.js"></script>
  <script src="/resources/bootstrap/multicolor/js/ready.js"></script>
  <div class="container">

<form class="form-inline">
    <div class="row">
    
       <h3>Sample 3</h3>
      <input type="text" id="unique-id" name="unique-name-3" data-palette='["#D50000","#304FFE","#00B8D4","#69F0AE","#FFFF00"]' value="#00B8D4">
      
      
    </div>
   </form>
</div>
    <button class="btn btn-success" type="button" id="reservationBtn">예약</button>
    <script>
    $("#reservationBtn").on("click",function(){
    	var color = $("#unique-id").val();
    	console.log(color);
    	
    })
    
    </script>
 


<%@include file="../include/footer.jsp"%>