<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- Main Content -->
<!--  삭제 처리를 위한 form 태그 작성 -->
<!-- 
<form role="form" method="post">
	<input type='hidden' name='ono' value="${OrderVO.ono }">
</form>
 -->

<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">예약페이지  
			
	
			</span>
			<div class="description">A order table for display list of
				data.</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">

						<div class="card-title">
							<div class="title">예약 현황</div> 
						</div>

					</div>
					<div class="card-body">
						<table class="table table-ordered">
							<tr>
								<th>Check<th>
								<th>ONO</th>
								<th>ID</th>
								<th>DNO</th>
								<th>RegDate</th>
								<th>OrderDate</th>
							</tr>

							<c:forEach items="${list }" var="orderVO">
								<tr>
								    <td><input type="checkbox"><td> 
									<td>${orderVO.ono }</td>
									<td>${orderVO.id }</td>
									<td>${orderVO.dno }</td>
									<td>${orderVO.regDate }</td>
									<td>${orderVO.orderDate }</td>
								</tr>
							</c:forEach>
						</table>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-warning"> 목록화면 </button>
						<button type="submit" class="btn btn-danger"> 삭제 </button>
					</div>
					<div class="text-center">
						<ul class="pagination">
						
						<c:if test="${pageMaker.prev}">
							<li><a href="listPage?page=${pageMaker.startPage -1}">&laquo;</a></li>
						</c:if>
						
						<c:forEach begin="${pageMaker.startPage}"
						end="${pageMaker.endPage }" var="idx">
						<li
						<c:out value="${pageMaker.cri.page == idx?' class=active':''}"/>>
						<a href="listPage?page=${idx}">${idx}</a>
						</li>
						</c:forEach>
						
						<c:if test="${pageMaker.next && pageMaker.endpage > 0}">
						<li><a href="listPage?page=${pageMaker.endPage+1}">&raquo;</a></li>
						</c:if>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../include/footer.jsp"%>