<%@ include file="../include/header.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%> 

<!-- Main Content -->
<!--  삭제 처리를 위한 form 태그 작성 -->
<form role="form" method="post">
	<input type='hidden' name='ono' value="${OrderVO.ono }">
</form>


<div class="container-fluid">
	<div class="side-body">
		<div class="page-title">
			<span class="title">예약페이지  
			
	
			</span>
			<div class="description">A order table for display list of
				data.</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="card">
					<div class="card-header">

						<div class="card-title">
							<div class="title">예약 현황</div> 
						</div>

					</div>
					<div class="card-body">
						<table class="table table-ordered">
							<tr>
								<th>Check<th>
								<th>ONO</th>
								<th>ID</th>
								<th>DNO</th>
								<th>RegDate</th>
								<th>OrderDate</th>
							</tr>

							<c:forEach items="${list }" var="OrderVO">
								<tr>
								    <td><input type="checkbox"><td> 
									<td>${OrderVO.ono }</td>
									<td>${OrderVO.id }</td>
									<td>${OrderVO.dno }</td>
									<td>${OrderVO.regDate }</td>
									<td>${OrderVO.orderDate }</td>
								</tr>
							</c:forEach>
						</table>
					</div>
					<div class="box-footer">
						<button type="submit" class="btn btn-warning"> 목록화면 </button>
						<button type="submit" class="btn btn-danger"> 삭제 </button>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<%@include file="../include/footer.jsp"%>