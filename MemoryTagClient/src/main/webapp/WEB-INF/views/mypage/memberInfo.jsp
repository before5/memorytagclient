<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<!-- Main Content -->
<!-- Main Content -->
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">회원정보수정</h1>
						<p>회원정보수정&nbsp; 페이지입니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">

	<div class="row">
		<div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form">
				<h2>
					회원정보수정&nbsp;<small>아래를 &nbsp;수정해주세요</small>
				</h2>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
							<input type="text" class="form-control" id="id"
								value='${id }' readonly="readonly">
						</div>
					</div>
					
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pw"
						value='${vo.pw }' placeholder="PW를 수정해주세요.." >
				</div>
				<div class="form-group">
				 <input type="email" class="form-control" id="email" value='${vo.email }' placeholder="Email을 수정해주세요..">
				</div>
				<div class="form-group">
					 <input  class="form-control" id="phone" value='${vo.phone }' placeholder="Phone Number를 수정해주세요..">
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" id="name"
								value='${vo.name }' placeholder="이름을 수정해주세요..">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<select
									name="month" id="gender" aria-controls="DataTables_Table_0"
									class="form-control input-md">
									
									<c:set var="gender" value='${vo.gender }'></c:set>
									<c:choose>
									<c:when test="${gender=='M'}">
									    <option value="M">남</option>
									    <option value="W">여</option>
									  </c:when>
									  <c:otherwise>
									     <option value="W">여</option>
									     <option value="M">남</option>
									  </c:otherwise>
									</c:choose>   
										</select>  
							</div>
					</div>
				</div>
				

				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<button type="button"
							class="btn btn-primary btn-block btn-lg" tabindex="7" id="back">돌아가기</button>
					</div>
					<div class="col-xs-12 col-md-6">
						<button type="button" class="btn btn-success btn-block btn-lg" id="modifyBtn" >수정</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<script>
	$(document).ready(function() {

		$("#back").on("click", function(event){
			self.location= "/";
		});
		
		$("#modifyBtn").on("click", function(event) {
			console.log("수정버튼");
			/* var year = $("#year option:selected").val().toString();
			var month = $("#month option:selected").val().toString();
			var day = $("#day option:selected").val().toString();
			var birth = year + month + day; */

			var obj = {
				id : $("#id").val(),
				pw : $("#pw").val(),
				name : $("#name").val(),
				email : $("#email").val(),
				gender : $("#gender option:selected").val().toString(),
				/* birth : birth, */
				phone : $("#phone").val()
			};
			$.ajax({
				type : 'post',
				url : '/mypage/memberInfo',
				headers : {
					"Content-Type" : "application/json"
				},
				dataType : 'text',
				data : JSON.stringify(obj),  //데이터를 JSON으로 넘겨준다
				success : function(result) {
					console.log(result);// reloadPage();
					alert("회원정보가 수정되었습니다.");
					self.location = "/mypage/myorder";
				}
			});
		});// 수정 버튼

	}); // document
</script>

<%@include file="../include/footer.jsp"%>