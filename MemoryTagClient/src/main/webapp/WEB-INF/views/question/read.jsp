<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<style>
@import "//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css";
.box > .icon { 
    text-align: 
    center; 
    position: 
    relative; 
    }
.box > .icon > .image { 
    position: relative; 
    z-index: 2; 
    margin: auto; 
    width: 88px; 
    height: 88px; 
    border: 8px solid white; 
    line-height: 88px; 
    border-radius: 50%; 
    background: #428bca; 
    vertical-align: middle; 
    }
.box > .icon > .image > i { 
    font-size: 36px !important; 
    color: #fff !important; 
    }
.box > .icon:hover > .image > i { 
    color: white !important; 
    }
.box > .icon > .info { 
    margin-top: -24px; 
    background: rgba(0, 0, 0, 0.04); 
    border: 1px solid #e0e0e0; 
    padding: 15px 0 10px 0; 
    }
.box > .icon > .info > h3.title { 
    font-family: sans-serif !important; 
    font-size: 16px; 
    color: #222; 
    font-weight: 500; 
    }
.box > .icon > .info > p { 
    font-family: sans-serif !important; 
    font-size: 13px; 
    color: #666; 
    line-height: 1.5em; 
    margin: 20px;
    }
.box > .icon:hover > .info > h3.title, .box > .icon:hover > .info > p, .box > .icon:hover > .info > .more > a { 
    color: #222; 
    }
.box > .icon > .info > .more a { 
    font-family: sans-serif !important; 
    font-size: 12px; 
    color: #222; 
    line-height: 12px; 
    text-transform: uppercase; 
    text-decoration: none; 
    }
.box > .icon:hover > .info > .more > a { 
    color: #fff; 
    padding: 6px 8px; 
    background-color: #63B76C; 
    }
.box .space { 
    height: 30px; 
    }

.modal-header-primary {
    color:#fff;
    padding:9px 15px;
    border-bottom:1px solid #eee;
    background-color: #428bca;
    -webkit-border-top-left-radius: 5px;
    -webkit-border-top-right-radius: 5px;
    -moz-border-radius-topleft: 5px;
    -moz-border-radius-topright: 5px;
     border-top-left-radius: 5px;
     border-top-right-radius: 5px;
}

</style>
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">Q & A</h1>
						<p>문의사항 페이지입니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">
				<form role="form" method="post">
					<input type="hidden" name="qno" value="${questionVO.qno}">
					<input type="hidden" name="family" value="${questionVO.family}">
					<input type="hidden" name="parent" value="${questionVO.parent}">
					<input type="hidden" name="depth" value="${questionVO.depth}">
					<input type="hidden" name="indent" value="${questionVO.indent}">
					<input type="hidden" name="page" value="${cri.page}">
					<input type="hidden" name="perPageNum" value="${cri.perPageNum}">
					<input type="hidden" name="searchType" value="${cri.searchType}">
					<input type="hidden" name="keyword" value="${cri.keyword}">
				</form>
				
				<div class="card-body">
					<form>
						<div class="form-group">
							<label for="exampleInputTitle">Title</label> <input type="text"
								class="form-control" id="exampleInputtitle"
								value="${questionVO.title}" readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputID">ID</label> <input type="text"
								class="form-control" id="exampleInputID"
								value="${questionVO.id}" readonly="readonly">
						</div>
						<div>
							<label for="exampleInputContent">Content</label>
							<textarea class="form-control" rows="13" readonly="readonly">${questionVO.content}</textarea>
						</div>
						<div class="text-right">
						<button type="submit" class="btn btn-warning" id="goListBtn">
							List</button>
							<button class="btn btn-success" type="button" id="reply">답글쓰기
					<span class="fa fa-pencil" aria-hidden="true"></span></button>
						<c:if test="${id == questionVO.id}"> 
							<button type="button" class="btn btn-primary" id="modifyBtn">Modify</button>
							<button type="submit" class="btn btn-danger" id="deleteBtn">Delete</button>
							
						</c:if>
					</div>
					</form>
					<div class="form-group">
						<label for="exampleInputName2">Replyer (ID)</label> <input
							type="text" class="form-control" id="replyer" name='replyID'
							value='${id }' readonly="readonly">
					</div>
					<div class="form-group">

						<label for="exampleInputTitle">Add Reply</label> <input
							type="text" class="form-control" id="replyText"
							placeholder="댓글을 작성해주세요..">
					</div>
					<div class="text-right">
					<button type="button" class="btn btn-warning" id="addReplyBtn">ADD
						Reply</button>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="primary" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header modal-header-primary">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">×</button>
				<h1 class="modal-title"></h1>
			</div>
			<div class="modal-body" data-rno>
				<input type="text" id="replytext" style="width: 100%; height: 100%;">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="modifyReplyBtn"
					data-dismiss="modal">modify</button>
				<button type="button" class="btn btn-danger" id="deleteReplyBtn"
					data-dismiss="modal">delete</button>
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
				
				
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Modal -->

<div class="container">
	<div class="row">
		<h1 class="title text-center wow fadeInDown animated"
			data-wow-duration="500ms" data-wow-delay="300ms"
			style="visibility: visible; animation-duration: 500ms; animation-delay: 300ms; animation-name: fadeInDown;">Comment</h1>
	</div>

</div>
<div class="container">
	<div class="row">
		<ul class="media-list">
           
		</ul>
	</div>
</div>
<div class="blog-pagination">
	<ul class="pagination">

	</ul>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script id="template" type="text/x-handlebars-template">
{{#each .}}
<li class="media" data-rno={{rno}}>
<div class="post-comment">
<div class="media-body">
<span><i class="fa fa-user"></i>{{replyer}}</span>
<p id="media-body2">{{replyText}}</p>
{{#eqReplyer replyer }}
<a class="btn btn-sm btn-success" data-toggle="modal" data-target="#primary">수정</a>
{{/eqReplyer}}
</div></div></li>
{{/each}}
</script>
<script>
function printPaging(pageMaker){
	console.log(pageMaker);
	 var str ="";

	 if(pageMaker.prev){
		 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous'><a href='"+(pageMaker.startPage-1)+"'> <span aria-hidden='true'> << </a></li>";
		 
	 }else if(!pageMaker.prev){
		 // prev가 없으면 
		 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous' style='visibility:hidden'><a href='"+(pageMaker.startPage-1)+"  onclick='return false;''> <span aria-hidden='true'> << </a></li>";
	 }

	 for(var i=pageMaker.startPage, len = pageMaker.endPage; i<=len; i++){

		 str += " <li class='paginate_button'><a href='"+i+"'>"+i+"</a></li>";
	 }
	 
	 if(pageMaker.next){
		
		 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous'><a href='"+(pageMaker.endPage+1)+"'> <span aria-hidden='true'> << </a></li>";
	 }else if(!pageMaker.next){
		 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous' style='visibility:hidden'><a href='"+(pageMaker.endPage+1)+"  onclick='return false;''> <span aria-hidden='true'> << </a></li>";
	 }
	 
	 $('.pagination').html(str);
};
Handlebars.registerHelper("eqReplyer", function(replyer, block) {
	var accum = '';
	if (replyer == '${id }') {
		accum += block.fn();
	}
	return accum;
});

$(document).ready(function() {
					var qno = ${questionVO.qno 	}; // qno를 선언했다.
					var id = $("#replyer").val(); // 사용자의 id
					var writer = $("#exampleInputID").val(); // 글 작성지
					console.log(id);
					
					$("#addReplyBtn").on("click",function(event) {
						event.preventDefault();
						console.log(qno);
						console.log($("#replyText").val());
						console.log($("#replyer").val());
						var obj = {qno : qno, replyText : $("#replyText").val(),replyer : $("#replyer").val()}
						// json타입으로 데이터를 보내주기 위한 준비 
						$.ajax({
							type : 'post',
							url : '/replies/registReply',
							headers : {"Content-Type" : "application/json"},
							dataType : 'text',
							data : JSON.stringify(obj),
							success : function(result) {
								console.log(result);// reloadPage();
								getPageList(1);
								}
							});
							$("#replyText").val("");
						//	$("#replyer").val(""); // 댓글 등록 완료되면 댓글 input칸 비우기!!!
						});
						/* 댓글 add버튼을 누르면 댓글이 달려야 한다. */
						var printData = function(replyArr,target,templateObject){
							
							var template = Handlebars.compile(templateObject.html());

							var html = template(replyArr);
							$(".media").remove();
							target.append(html);
						};
									
						// 처음 들어왔을 때 댓글 첫번째 페이지가 보여야한다. page번호를 주면 댓글이 나오는 페이지 
						function getPageList(page) {
							$.getJSON("/replies/" + qno + "/" + page,function(data) {
								
								var pageMaker = data.pageMaker;
								printData(data.replyList, $(".media-list"), $('#template'));
								printPaging(pageMaker);
							
						});
						};
						getPageList(1);
						
						$(".pagination").on("click","li a",function(event){
							event.preventDefault();
							replyPage = $(this).attr("href");
							console.log(replyPage);
							getPageList(replyPage);
						});// 댓글 페이지를 클릭했을 때 알맞을 댓글이 갱신되는 페이지 
						
						var formObj= $("form[role='form']"); // 위에 hidden으로 되어있는 페이지와 question객체
						console.log(formObj);
						
						
						// 문의사항 삭제 메세지
						$("#deleteBtn").on("click",function(event){
							event.preventDefault();
							//json으로 보낸다. 
							$.ajax({
								type : 'get',
								url : '/question/checkParent/' + qno,
								headers : {
									"Content-Type" : "application/json"
								},
								dataType : 'text',
								success : function(result) {
									console.log(result);
									var status = result;
									if (status == 'parent') {
										alert("아래의 답글이 있으므로 삭제가 불가능합니다.");
										
									} else {
										alert("삭제되었습니다.");
										 formObj.attr("action","/question/delete");
										formObj.submit();
									}
								}
							});
							 
							  
						});
						//list버튼을 눌렀을 때 
						$("#goListBtn").on("click",function(event){
							event.preventDefault();
							formObj.attr("method", "get");
							formObj.attr("action", "/question/list");
							formObj.submit();
						});
						
						
						// 댓글 클릭했을 때 위에 올리는것 
						$(".media-list").on("click",".media",function(event){
							var reply = $(this);
							
							$("#replytext").val(reply.find('#media-body2').text());
							$(".modal-title").html(reply.attr("data-rno"));
						});
						
						//댓글 수정 버튼 
						$("#modifyReplyBtn").on("click",function(event){
							event.preventDefault();
							var rno = $(".modal-title").html();
							
							 $("input[name=replyID]").attr("disabled",false);
							
							var obj = {qno : qno, replyText : $("#replytext").val(), rno:rno}
							// json타입으로 데이터를 보내주기 위한 준비 
							$.ajax({
								type : 'put',
								url : '/replies/'+rno,
								headers : {"Content-Type" : "application/json"},
								dataType : 'text',
								data : JSON.stringify(obj),
								success : function(result) {
									console.log(result);// reloadPage();
									getPageList(1);
									}
								});
							 $("#replyText").val("");
							//	$("#replyer").val("");
						});
						
						//댓글 삭제 버튼
						$("#deleteReplyBtn").on("click",function(event){
							event.preventDefault();
							var rno = $(".modal-title").html();
							console.log(rno);
							$.ajax({
								type : 'delete',
								url : '/replies/'+rno,
								headers : {"Content-Type" : "application/json"},
								dataType : 'text',
								data : JSON.stringify(rno),
								success : function(result) {
									console.log(result);// reloadPage();
									getPageList(1);
									}
								});
							 $("#replyText").val("");
							//	$("#replyer").val("");
						});
					
						var formObj = $("form[role='form']");
						$("#modifyBtn").on("click",function(){
							if(id != writer){
								alert("수정할 권한이 없습니다.");
							}else{
								formObj.attr("action", "/question/modify");
								formObj.attr("method", "get");
								formObj.submit();
							}
						});
						
						$("#reply").on("click",function(event){
							// 답글달기 버튼을 누르면 
							formObj.attr("action", "/question/addReply");
							formObj.attr("method", "get");
							formObj.submit();
						});
					});
	
</script>
<%@include file="../include/footer.jsp"%>