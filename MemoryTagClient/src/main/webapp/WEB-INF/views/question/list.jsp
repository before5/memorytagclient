<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">Q & A</h1>
						<p>문의사항 페이지입니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
<div class="description">
				<select name="searchType">
					<option value="n"
						<c:out value="${cri.searchType == null?'selected':''}"/>>
						Search</option>
					<option value="t"
						<c:out value="${cri.searchType eq 't'?'selected':''}"/>>
						Title</option>
					<option value="c"
						<c:out value="${cri.searchType eq 'c'?'selected':''}"/>>
						Content</option>
					<option value="i"
						<c:out value="${cri.searchType eq 'i'?'selected':''}"/>>
						ID</option>
					<option value="tc"
						<c:out value="${cri.searchType eq 'tc'?'selected':''}"/>>
						Title OR Content</option>
					<option value="ci"
						<c:out value="${cri.searchType eq 'ci'?'selected':''}"/>>
						Content OR ID</option>
					<option value="tci"
						<c:out value="${cri.searchType eq 'tci'?'selected':''}"/>>
						Title OR Content OR ID</option>

				</select> <input type="text" name='keyword' id="keywordInput"
					value="${cri.keyword}">
				<button class="btn btn-info" id="searchBtn">Search</button>
			</div>
		

	<div class="col-xs-12">
		<table class="table table-striped custab">
			<thead>

				<tr>
					<th>Number</th>
					<th>Title</th>
					<th>ID</th>
					<th>RegisterDate</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${list}" var="questionVO">
					<tr>
						<td>${questionVO.qno}</td>
							<c:if test="${questionVO.indent > 0 }">
							 <td><a href='read${pageMaker.makeSearch(pageMaker.cri.page) }&qno=${questionVO.qno }'>
							 <c:forEach var="i" begin="2" end="${questionVO.indent }" step="1">
						&nbsp;&nbsp; &nbsp;&nbsp;
						</c:forEach>
							 	 ㄴ${questionVO.title}<strong> &nbsp;&nbsp;[${questionVO.replycnt }]</strong></a></td>
						 	</c:if>	
					<c:if test="${questionVO.indent == 0 }">
						  <td><a href='read${pageMaker.makeSearch(pageMaker.cri.page) }&qno=${questionVO.qno }'>
						  ${questionVO.title}<strong>&nbsp;&nbsp;[${questionVO.replycnt }]</strong></a></td>
						 	</c:if>	
						
						<td>${questionVO.id}</td>
						<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
								value="${questionVO.regdate}" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</div>

<div class="container">
	
	<div class="col-xs-12">
	<div class="text-right">
		<ul class="list-inline" id="options">
			<li class="option">
				
				<button class="btn btn-success" type="button" id="registerBtn">글쓰기
					<span class="fa fa-pencil" aria-hidden="true"></span>
				</button>

			</li>
		</ul>
	</div>
</div>
</div>


<!-- 댓글 페이지 번호 나오는 곳 -->

<div class="blog-pagination">

	<ul class="pagination">
		<c:choose>
			<c:when test="${pagemaker.prev}">
				<li ><a
					href="list${pagemaker.makeSearchURI(pagemaker.startPage-1)} "
					aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
				</a></li>
			</c:when>
			<c:otherwise>
				<li style="visibility:hidden"><a
					href="list${pageMaker.makeSearchURI(pagemaker.startPage-1)} "
					aria-label="Previous"> <span aria-hidden="true"
						onclick="return false;">&laquo;</span>
				</a></li>
			</c:otherwise>
		</c:choose>

		<c:forEach begin="${pageMaker.startPage}" end="${pageMaker.endPage}"
			var="idx">
			<li
				<c:out value="${pageMaker.cri.page == idx?'class = active':'' }"/>>
				<a href="list${pageMaker.makeSearchURI(idx) }">${idx}</a></li>
		</c:forEach>
		<c:choose>
			<c:when test="${pageMaker.next && pageMaker.endPage>0}">
				<li><a
					href="list${pageMaker.makeSearchURI(pageMaker.endPage+1)} "
					aria-label="Next"> <span aria-hidden="true">&raquo;</span>
				</a></li>
			</c:when>
			<c:otherwise>
				<li style="visibility:hidden"><a
					href="list${pageMaker.makeSearchURI(pageMaker.endPage+1)} "
					aria-label="Next"> <span aria-hidden="true"
						onclick="return false;">&raquo;</span>
				</a></li>
			</c:otherwise>
		</c:choose>
	</ul>
</div>

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<script>
$(document).ready(function(){
	$("#registerBtn").on("click",function(){
		self.location = "/question/register";
	});
	
	$("#searchBtn").on("click",function(event){
		
		self.location = "list" 
						+ '${pageMaker.makeQuery(1)}'
						+"&searchType="
						+$("select option:selected").val()
						+"&keyword="+$("#keywordInput").val();
	});
});
</script>
<%@include file="../include/footer.jsp"%>