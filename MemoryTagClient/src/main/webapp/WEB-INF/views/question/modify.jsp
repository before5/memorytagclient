<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">Q & A</h1>
						<p>문의사항 수정 페이지입니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="row">
		<div class="col-xs-12">
			<div class="card">

				<div class="card-body">
					<form role="form" method="post">
						<input type="hidden" value='${questionVO.qno }' name="qno">
						<div class="form-group">
							<label for="exampleInputTitle">Title</label> <input type="text"
								class="form-control" id="exampleInputtitle" name="title" value='${questionVO.title }'>
						</div>
						<div class="form-group">
							<label for="exampleInputID">ID</label> <input type="text"
								class="form-control" id="exampleInputID" value="${id}"
								readonly="readonly" name="id">
						</div>
						<div class="form-group">
							<label for="exampleInputContent">Content</label>
							<textarea class="form-control" rows="13" name="content">${questionVO.content }</textarea>
						</div>
						<div class="text-right">
							<div class="card-body">
								<button type="submit" class="btn btn-warning" id="okBtn">확인</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script>

</script>

<%@include file="../include/footer.jsp"%>