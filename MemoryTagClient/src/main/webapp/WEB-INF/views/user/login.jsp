
<%@ include file="../include/header.jsp"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!-- Main Content -->
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">Login</h1>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container"  style="display: inline-block;text-align: center;">
<div class="col-md-6"></div>
<div class="col-md-4 col-sm-12">            
		<div class="contact-form bottom">
			<h3>LOGIN해주세요.</h3>
        
			<form id="main-contact-form" name="contact-form" method="post"
				action="/user/loginPost">
				<div class="form-group" >
					<input type="text" class="form-control" name="id"
						placeholder="ID를 입력하세요" id="inputEmail3">
				</div>
				<div class="form-group">
					<input type="password" class="form-control" name="pw"
						id="inputPassword3" placeholder="PW를 입력하세요">
				</div>
				<div class="form-group">
					<input type="checkbox" id="checkbox-10" name="useCookie"> <label
						for="checkbox-10"> Remember me </label>

				</div>
				<div class="form-group">
					<button class="btn btn-primary">Login</button>
					<button type="button" class="btn btn-warning" id="joinBtn">회원가입</button>
				</div>
			</form>
		</div>
	</div>
</div>
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script>
$(document).ready(function(){
	$("#joinBtn").on("click",function(event){
		event.preventDefault();
		self.location="/member/join";
	});
});

</script>
<%@ include file="../include/footer.jsp"%>