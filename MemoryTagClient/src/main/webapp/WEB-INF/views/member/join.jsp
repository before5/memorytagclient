<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@include file="../include/header.jsp"%>
<!-- Main Content -->
<!-- Main Content -->
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">회원가입</h1>
						<p>회원가입 페이지입니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">

	<div class="row">
		<div
			class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
			<form role="form">
				<h2>
					회원가입 <small>아래를 채워주세요</small>
				</h2>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" id="id"
								placeholder="ID를 입력해주세요">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">

							<button type="button" class="btn btn-success" id="idcBtn">id
								check</button>
						</div>
					</div>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pw"
						placeholder="PW를 입력해주세요..">
				</div>
				<div class="form-group">
				 <input type="email" class="form-control" id="email" placeholder="Email">
				</div>
				<div class="form-group">
					 <input  class="form-control" id="phone" placeholder="Phone Number" >
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<input type="text" class="form-control" id="name"
								placeholder="이름">
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="form-group">
							<select
									name="month" id="gender" aria-controls="DataTables_Table_0"
									class="form-control input-md">
									<option value="M">남</option>
									<option value="W">여</option>
									</select>
						</div>
					</div>
				</div>
				

				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-12 col-md-6">
						<input type="button" value="돌아가기"
							class="btn btn-primary btn-block btn-lg" tabindex="7" id="back">
					</div>
					<div class="col-xs-12 col-md-6">
						<button type="button" class="btn btn-success btn-block btn-lg" id="regBtn" >등록</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<script>
	$(document).ready(function() {

		$("#idcBtn").on("click", function(event) {
			console.log("id 체크버튼");
			event.preventDefault();
			var id = $("#id").val();

			$.ajax({
				type : 'get',
				url : '/member/check/' + id,
				headers : {
					"Content-Type" : "application/json"
				},
				dataType : 'text',
				success : function(result) {
					console.log(result);
					var status = result;
					if (status == 'find') {
						alert("이미 사용중인 id입니다.");
						$("#id").val(" ");
					} else {
						alert("사용할 수 있는 id입니다.");
					}
				}
			});
		});// 체크버튼
		$("#regBtn").on("click", function(event) {
			console.log("등록버튼");
			/* var year = $("#year option:selected").val().toString();
			var month = $("#month option:selected").val().toString();
			var day = $("#day option:selected").val().toString();
			var birth = year + month + day; */

			var obj = {
				id : $("#id").val(),
				pw : $("#pw").val(),
				name : $("#name").val(),
				email : $("#email").val(),
				gender : $("#gender option:selected").val().toString(),
				/* birth : birth, */
				phone : $("#phone").val()
			};
			$.ajax({
				type : 'post',
				url : '/member/join',
				headers : {
					"Content-Type" : "application/json"
				},
				dataType : 'text',
				data : JSON.stringify(obj),
				success : function(result) {
					console.log(result);// reloadPage();
					alert("회원가입이 완료되었습니다.");
					self.location = "/user/login";
				}
			});
		});// 등록 버튼

	}); // document
</script>

<%@include file="../include/footer.jsp"%>