<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<title>Home | Triangle</title>


<link href="/resources/bootstrap/multicolor/css/bootstrap.min.css"
	rel="stylesheet">
<link href="/resources/bootstrap/multicolor/css/font-awesome.min.css"
	rel="stylesheet">
<link href="/resources/bootstrap/multicolor/css/animate.min.css"
	rel="stylesheet">
<link href="/resources/bootstrap/multicolor/css/lightbox.css"
	rel="stylesheet">
<link href="/resources/bootstrap/multicolor/css/main.css"
	rel="stylesheet">
<link href="/resources/bootstrap/multicolor/css/responsive.css"
	rel="stylesheet">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script src="/resources/bootstrap/multicolor/js/bootstrap.min.js"></script>
<!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->
<link rel="shortcut icon"
	href="/resources/bootstrap/multicolor/images/ico/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="144x144"
	href="/resources/bootstrap/multicolor/images/ico/apple-touch-icon-144-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="114x114"
	href="/resources/bootstrap/multicolor/images/ico/apple-touch-icon-114-precomposed.png">
<link rel="apple-touch-icon-precomposed" sizes="72x72"
	href="/resources/bootstrap/multicolor/images/ico/apple-touch-icon-72-precomposed.png">
<link rel="apple-touch-icon-precomposed"
	href="/resources/bootstrap/multicolor/images/ico/apple-touch-icon-57-precomposed.png">
</head>
<!--/head-->

<body>

	<header id="header">      
        <div class="container">
            <div class="row">
                <div class="col-sm-12 overflow">
                   <div class="social-icons pull-right">
                        <ul class="nav nav-pills">
                         <c:if test="${empty login}">
                         <a href="/user/login" class="btn btn-common">Login</a>
                        </c:if>
                        
                        <c:if test="${not empty login}">
                       <li>${login.id }님 환영합니다.</li>
                     <a href="/user/logout" class="btn btn-common">Logout</a>
                        </c:if>
                           
                        </ul>
                    </div> 
                </div>
             </div>
        </div>
        <div class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>


                    <a class="navbar-brand" href="/">
                    	<h1><img src="/resources/bootstrap/multicolor/images/logo.png" alt="logo"></h1>
                    </a>
                    
                </div>
               
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        
                        <li ><a href="/reservation/reserve"><h3>예약하기</h3></a></li>
                        <li><a href="/question/list"><h3>문의사항</h3></a></li> 
                        <li><a href="/notice/listPage"><h3>공지사항</h3></a></li>
                    <li class="dropdown"><a href="#"><h3>마이페이지 </h3><i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="/mypage/memberInfo"><h3>회원정보수정</h3></a></li>
                                <li><a href="/mypage/myorder"><h3>나의 주문내역</h3></a></li>
                               
                            </ul>   
                            </li>  
                           
                        
                   
                   
                          <c:if test="${loginVO.grade == 1}"> 
                            <li class="dropdown"><a href="#"><h3>관리 </h3><i class="fa fa-angle-down"></i></a>
                            <ul role="menu" class="sub-menu">
                                <li><a href="/admin/list"><h3>회원관리</h3></a></li>
                                <li><a href="/order/list"><h3>예약관리</h3></a></li>
                                <li><a href="/question/list"><h3>문의사항</h3></a></li>
                                <li><a href="/notice/listPage"><h3>공지사항</h3></a></li>
                            </ul>   
                            </li>  
                            </c:if>           
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!--/#header-->

