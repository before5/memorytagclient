<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ include file="../include/header.jsp"%>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
	<script src="/resources/bootstrap/multicolor/js/dropzone.min.js"></script> <!-- 경로가 변하면 src 결로 바꾸기 -->
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">Notice</h1>
						<p>공지사항 페이지입니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="side-body">
		

		<div class="row">
			<div class="col-xs-12">
				<div class="card">

					<div class="card-body">
							
							<div class="form-group">

						<input type="hidden" name="nno" value="${notice.nno}"> 
						<input type="hidden" name="page" value="${cri.page}"> 
						<input type="hidden" name="perPageNum" value="${cri.perPageNum}">
						<input type="hidden" name="searchType" value="${cri.searchType}">
						<input type="hidden" name="keyword" value="${cri.keyword}">
						
								<label for="exampleInputNno">Number</label> <input type="text"
									class="form-control" id="exampleInputNno" name="nno"
									value="${notice.nno}" readonly="readonly">
							</div>
							
							<div class="form-group">
								<label for="exampleInputTitle">Title</label> <input type="text"
									class="form-control" id="exampleInputtitle" name="title"
									value="${notice.title}">
							</div>
							<div class="form-group">
								<label for="exampleInputID">ID</label> <input type="text"
									class="form-control" id="exampleInputID" value="${notice.id}"
									name="id" readonly="readonly">
							</div>
							<div>
								<label for="exampleInputContent">Content</label>
								<textarea class="form-control" rows="13" name="content" id="noticeContent">${notice.content}</textarea>
							</div>



							<div class="form-group">

							<form enctype="multipart/form-data" id="my-awesome-dropzone">
								<label for="exampleInputFile">File input</label>
								<div class="dropzone" id="myDropzone"></div>
								<p class="help-block"></p>
							</form>
						</div>
					
				</div>
				<div class="box-footer">
					<button type="submit" class="btn btn-warning">Cancel</button>

					<button type="submit" class="btn btn-default">Save</button>
				</div>


				<script>
					$(document).ready(function() {

					
						Dropzone.formData = new FormData();
						//커스텀 dropzone생성
						Dropzone.options.myDropzone = {
								url : "/notice/modifyPage",
								autoProcessQueue : false,
								uploadMultiple : true,
								parallelUploads : 10,
								maxFiles : 10,//최대 파일 업로드 갯수
								addRemoveLinks : true,
								init : function(){
									dzClosure = this; // Makes sure that 'this' is understood inside the functions below.
									// for Dropzone to process the queue (instead of default form behavior):
									
									this.on("addedfile", function(file) {//파일이 추가되면 이벤트가 발생
										console.log(file.name);
									});
									$(".btn-default").on("click", function(e) {
										e.preventDefault();
										e.stopPropagation();
										dzClosure.processQueue(); //파일전송
										
									});
									this.on("sendingmultiple",
											function(data, xhr, formData) {
												formData.append("nno",$("#exampleInputNno").val());
												console.log($("#exampleInputNno").val());
												formData.append("title", $(
														"#exampleInputtitle").val());
												formData.append("id", $(
														"#exampleInputID").val());
												formData.append("content", $(
														"#noticeContent").val());
												
											});
									this.on("successmultiple", function(){
										location.href = '/notice/listPage';
									});
								}
						}

					

						$(".btn-warning").on("click", function() {
							self.location = "/notice/listPage?page=${cri.page}&perPageNum=${cri.perPageNum}"+"&sesarchType=${cri.searchType}&keyword=${cri.keyword}";
						});

						

					});
						
				</script>
			</div>
		</div>
	</div>


</div>


<%@ include file="../include/footer.jsp"%>