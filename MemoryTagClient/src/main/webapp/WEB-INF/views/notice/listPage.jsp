<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ include file="../include/header.jsp"%>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<!-- Main Content -->
<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">Notice</h1>
						<p>공지사항 페이지입니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="side-body">
		<div class="page-title">

			<div class="description">
				<select name="searchType">
					<option value="n"
						<c:out value="${cri.searchType == null?'selected':''}"/>>
						Search</option>
					<option value="t"
						<c:out value="${cri.searchType eq 't'?'selected':''}"/>>
						Title</option>
					<option value="c"
						<c:out value="${cri.searchType eq 'c'?'selected':''}"/>>
						Content</option>
					<option value="i"
						<c:out value="${cri.searchType eq 'i'?'selected':''}"/>>
						ID</option>
					<option value="tc"
						<c:out value="${cri.searchType eq 'tc'?'selected':''}"/>>
						Title OR Content</option>
					<option value="ci"
						<c:out value="${cri.searchType eq 'ci'?'selected':''}"/>>
						Content OR ID</option>
					<option value="tci"
						<c:out value="${cri.searchType eq 'tci'?'selected':''}"/>>
						Title OR Content OR ID</option>

				</select> <input type="text" name='keyword' id="keywordInput"
					value="${cri.keyword}">
				<button class="btn btn-info ">Search</button>
			</div>
		</div>

		<div class="col-xs-12">



			<table class="table table-striped custab">
				<thead>
					<tr>
						<th>Number</th>
						<th>Title</th>
						<th>ID</th>
						<th>RegisterDate</th>
						<th>UpdateDate</th>
						<th>ViewCount</th>

					</tr>
				</thead>
				<tbody>
					<c:forEach items="${list }" var="noticeVO">
						<tr>



							<td>${noticeVO.nno }</td>
							<td><a
								href='/notice/readPage${pagemaker.makeURI(pagemaker.cri.page)}&nno=${noticeVO.nno }'>${noticeVO.title }</a></td>

							<td><a href=''>${noticeVO.id }</a></td>
							<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
									value="${noticeVO.regdate }" /></td>

							<td><fmt:formatDate pattern="yyyy-MM-dd HH:mm"
									value="${noticeVO.updatedate}" /></td>
							<td>${noticeVO.viewcnt }</td>

						</tr>
					</c:forEach>
				</tbody>
			</table>



		</div>
	</div>
</div>

<div class="container">

	<div class="col-xs-12">
		<div class="text-right">
		 <c:if test="${loginVO.grade == 1}"> 
			<ul class="list-inline" id="options">
				<li class="option">
					<button class="btn btn-success" type="button" id="registerBtn">
						글쓰기 <span class="fa fa-pencil" aria-hidden="true"></span>
					</button>
				</li>
			</ul>
			</c:if>
		</div>
	</div>
</div>

<div class="blog-pagination">

	<ul class="pagination">
		<c:choose>
			<c:when test="${pagemaker.prev}">
				<li><a
					href="listPage${pagemaker.makeSearchURI(pagemaker.startPage-1)} "
					aria-label="Previous"> <span aria-hidden="true">&laquo;</span>
				</a></li>
			</c:when>
			<c:otherwise>
				<li style="visibility:hidden"><a
					href="listPage${pagemaker.makeSearchURI(pagemaker.startPage-1)} "
					aria-label="Previous"> <span aria-hidden="true"
						onclick="return false;">&laquo;</span>
				</a></li>
			</c:otherwise>
		</c:choose>

		<c:forEach begin="${pagemaker.startPage}" end="${pagemaker.endPage}"
			var="idx">
			<li
				<c:out value="${pagemaker.cri.page == idx?'class = active':'' }"/>>
				<a href="listPage${pagemaker.makeSearchURI(idx) }">${idx}</a></li>
		</c:forEach>
		<c:choose>
			<c:when test="${pagemaker.next && pagemaker.endPage>0}">
				<li><a
					href="listPage${pagemaker.makeSearchURI(pagemaker.endPage+1)} "
					aria-label="Next"> <span aria-hidden="true">&raquo;</span>
				</a></li>
			</c:when>
			<c:otherwise>
				<li style="visibility:hidden"><a
					href="listPage${pagemaker.makeSearchURI(pagemaker.endPage+1)} "
					aria-label="Next"> <span aria-hidden="true"
						onclick="return false;">&raquo;</span>
				</a></li>
			</c:otherwise>
		</c:choose>
	</ul>
</div>



<script>
$(document).ready(function(){
	$(".btn-info").on("click",function(event){
		event.preventDefault();
		self.location = "listPage"
		+ "${pagemaker.makeURI(1)}"  // 검색할 때에는 makeURI를 쓴다.
		+ "&searchType="
		+ $("select option:selected").val() // 선택한 조건을 갖고온다.
		+ "&keyword=" + $('#keywordInput').val(); // 입력한 키워드를 갖고온다.
		

	});
	$("#registerBtn").on("click",function(){
		self.location = "/notice/register";
	});
});
</script>

<%@ include file="../include/footer.jsp"%>