<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.css">
<%@ include file="../include/header.jsp"%>
<style>
@import
	"//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css"
	;

.box>.icon {
	text-align: center;
	position: relative;
}

.box>.icon>.image {
	position: relative;
	z-index: 2;
	margin: auto;
	width: 88px;
	height: 88px;
	border: 8px solid white;
	line-height: 88px;
	border-radius: 50%;
	background: #428bca;
	vertical-align: middle;
}

.box>.icon>.image>i {
	font-size: 36px !important;
	color: #fff !important;
}

.box>.icon:hover>.image>i {
	color: white !important;
}

.box>.icon>.info {
	margin-top: -24px;
	background: rgba(0, 0, 0, 0.04);
	border: 1px solid #e0e0e0;
	padding: 15px 0 10px 0;
}

.box>.icon>.info>h3.title {
	font-family: sans-serif !important;
	font-size: 16px;
	color: #222;
	font-weight: 500;
}

.box>.icon>.info>p {
	font-family: sans-serif !important;
	font-size: 13px;
	color: #666;
	line-height: 1.5em;
	margin: 20px;
}

.box>.icon:hover>.info>h3.title, .box>.icon:hover>.info>p, .box>.icon:hover>.info>.more>a
	{
	color: #222;
}

.box>.icon>.info>.more a {
	font-family: sans-serif !important;
	font-size: 12px;
	color: #222;
	line-height: 12px;
	text-transform: uppercase;
	text-decoration: none;
}

.box>.icon:hover>.info>.more>a {
	color: #fff;
	padding: 6px 8px;
	background-color: #63B76C;
}

.box .space {
	height: 30px;
}

.modal-header-primary {
	color: #fff;
	padding: 9px 15px;
	border-bottom: 1px solid #eee;
	background-color: #428bca;
	-webkit-border-top-left-radius: 5px;
	-webkit-border-top-right-radius: 5px;
	-moz-border-radius-topleft: 5px;
	-moz-border-radius-topright: 5px;
	border-top-left-radius: 5px;
	border-top-right-radius: 5px;
}
</style>
<!-- Main Content -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/3.0.1/handlebars.js"></script>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<script id="template" type="text/x-handlebars-template">
{{#each .}}

<li class="media" data-nrno={{nrno}}>

<div class="post-comment">
<div class="media-body">
<span><i class="fa fa-user"></i>{{id}}</span>
<p id="media-body2">{{replytext}}</p>
{{#eqReplyer id}}
<a class="btn btn-sm btn-success" data-toggle="modal" data-target="#primary">수정</a> 
{{/eqReplyer}}
</div></div></li>
{{/each}}
</script>

<section id="page-breadcrumb">
	<div class="vertical-center sun">
		<div class="container">
			<div class="row">
				<div class="action">
					<div class="col-sm-12">
						<h1 class="title">Notice</h1>
						<p>공지사항 페이지입니다.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container">
	<div class="side-body">
		<div class="row">
			<div class="col-xs-12">
				<div class="card">

					<form role="form" method="post">
						<input type="hidden" name="nno" value="${notice.nno}"> <input
							type="hidden" name="page" value="${cri.page}"> <input
							type="hidden" name="perPageNum" value="${cri.perPageNum}">
						<input type="hidden" name="searchType" value="${cri.searchType}">
						<input type="hidden" name="keyword" value="${cri.keyword}">
					</form>

					<div class="card-body">

						<div class="form-group">
							<label for="exampleInputTitle">Title</label> <input type="text"
								class="form-control" id="exampleInputtitle"
								value="${notice.title}" readonly="readonly">
						</div>
						<div class="form-group">
							<label for="exampleInputID">ID</label> <input type="text"
								class="form-control" id="exampleInputID" value="${notice.id}"
								readonly="readonly">
						</div>
						<div>
							<label for="exampleInputContent">Content</label>
							<textarea class="form-control" rows="13" readonly="readonly">${notice.content}</textarea>
						</div>


						<div class="form-group">
							<form enctype="multipart/form-data" id="my-awesome-dropzone">
								<label for="exampleInputFile">Attached File</label>
								<div class="dropzone" id="myDropzone">
									<c:forEach items="${attach }" var="file">
										<label>
											<div>

												<c:choose>
													<c:when test="${file.extension=='image/jpeg' }">
														<img
															src='/notice/displayFile?fileName=${file.stored_file_name }' />
													</c:when>
													<c:otherwise>
														<img src="/resources/bootstrap/img/file.png" />
													</c:otherwise>
												</c:choose>

											</div> <a
											href='/notice/displayFile?fileName=${file.stored_file_name }'>
												${file.original_file_name } </a>
										</label>
									</c:forEach>

								</div>
								<p class="help-block"></p>
							</form>
						</div>
					</div>
					<div class="box-footer">
						<c:if test="${loginVO.grade == 1}">
							<button type="submit" class="btn btn-warning" id="modify">Modify</button>
							<button type="submit" class="btn btn-danger" id="delete">Delete</button>
							<button type="submit" class="btn btn-primary" id="list">List
						Page</button>
						</c:if>
					</div>


					<!--<button type="submit" class="btn btn-default">List All</button>-->
					

					<div class="form-group">
						<label for="exampleInputName2">Replyer (ID)</label> <input
							type="text" class="form-control" id="replyer" name='replyID'
							value='${id }' readonly="readonly">

					</div>
					<div class="form-group">

						<label for="exampleInputTitle">Add Reply</label> <input
							type="text" class="form-control" id="replyText"
							placeholder="댓글을 작성해주세요..">
					</div>
					<button type="button" class="btn btn-success" id="addReplyBtn">ADD
						Reply</button>




				</div>
			</div>
		</div>


	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="primary" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header modal-header-primary">
				<button type="button" class="close" data-dismiss="modal"
					aria-hidden="true">×</button>
				<h1 class="modal-title"></h1>
			</div>

			<div class="modal-body" data-nrno>
				<input type="text" id="replytext" style="width: 100%;">

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" id="modifyReplyBtn"
					data-dismiss="modal">modify</button>
				<button type="button" class="btn btn-danger" id="deleteReplyBtn"
					data-dismiss="modal">delete</button>
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- Modal -->
<div class="container">
	<div class="row">
		<h1 class="title text-center wow fadeInDown animated"
			data-wow-duration="500ms" data-wow-delay="300ms"
			style="visibility: visible; animation-duration: 500ms; animation-delay: 300ms; animation-name: fadeInDown;">Comment</h1>
	</div>

</div>
<div class="container">
	<div class="row">
		<ul class="media-list">

		</ul>
	</div>
</div>
<div class="blog-pagination">
	<ul class="pagination">

	</ul>
</div>

<script>
	Handlebars.registerHelper("eqReplyer", function(replyer, block) {
		var accum = '';
		if (replyer == '${id }') {
			accum += block.fn();
		}
		return accum;
	});
	
	$(document)
			.ready(
					function() {

						var formObj = $("form[role='form']");

						console.log(formObj);

						$("#modify").on("click", function() {
							formObj.attr("action", "/notice/modifyPage");
							formObj.attr("method", "get");
							formObj.submit();
						});

						$("#delete").on("click", function() {

							//formObj.attr("nno", "32");
							formObj.attr("action", "/notice/deletePage");
							formObj.submit();

						});

						//$(".btn-default")                자바스크립트의 주석 //
						//		.on(						html에서의 주석 <!-- --> 주석 처리 제대로 하기
						//				"click",
						//				function() {
						//					self.location = "/notice/listAll";
						//				});

						$("#list").on("click", function() {
							formObj.attr("action", "/notice/listPage");
							formObj.attr("method", "get");
							formObj.submit();
						});

						function checkImageType(fileName) {
							var pattern = /jpg|gif|png|jpeg/i;
							return fileName.match(pattern);
						}

						Handlebars.registerHelper("prettifyDate", function(
								timeValue) {
							var dateObj = new Date(timeValue);
							var year = dateObj.getFullYear();
							var month = dateObj.getMonth() + 1;
							var date = dateObj.getDate();
							return year + "/" + month + "/" + date;
						});

						var printData = function(replyArr, target,
								templateObject) {

							var template = Handlebars.compile(templateObject
									.html());

							var html = template(replyArr);

							console.log(html)
							$(".media").remove();//기존리스트를 지운다.
							target.append(html); //append는 ul안에 li를 붙인ㄷ

						}

						var nno = ${notice.nno };

						var replyPage = 1;

						function getPage(pageInfo) {
							$.getJSON(pageInfo, function(data) {
								printData(data.list, $(".media-list"),
										$('#template'));
								printPaging(data.pageMaker, $(".pagination"));

								//$("#modifyModal").modal('hide');

							});
						}

						getPage("/nreplies/" + nno + "/" + 1);
						function printPaging(pageMaker){
							console.log(pageMaker);
							 var str ="";

							 if(pageMaker.prev){
								 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous'><a href='"+(pageMaker.startPage-1)+"'> <span aria-hidden='true'> << </a></li>";
								 
							 }else if(!pageMaker.prev){
								 // prev가 없으면 
								 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous' style='visibility:hidden'><a href='"+(pageMaker.startPage-1)+"  onclick='return false;''> <span aria-hidden='true'> << </a></li>";
							 }

							 for(var i=pageMaker.startPage, len = pageMaker.endPage; i<=len; i++){

								 str += " <li class='paginate_button'><a href='"+i+"'>"+i+"</a></li>";
							 }
							 
							 if(pageMaker.next){
								
								 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous'><a href='"+(pageMaker.endPage+1)+"'> <span aria-hidden='true'> << </a></li>";
							 }else if(!pageMaker.next){
								 str +="<li class='paginate_button previous' id='DataTables_Table_0_previous' style='visibility:hidden'><a href='"+(pageMaker.endPage+1)+"  onclick='return false;''> <span aria-hidden='true'> << </a></li>";
							 }
							 
							 $('.pagination').html(str);
						};

						



						$(".pagination").on("click", "li a", function(event) {

							event.preventDefault();

							replyPage = $(this).attr("href");

							getPage("/nreplies/" + nno + "/" + replyPage);

						});
						
						// 댓글 클릭했을 때 위에 올리는것 
						$(".media-list").on("click",".media",function(event){
							var reply = $(this);
							console.log(reply)
							$("#replytext").val(reply.find('#media-body2').text());
							$(".modal-title").html(reply.attr("data-nrno"));
						});
						
						$("#addReplyBtn")
								.on(
										"click",
										function(event) {

											var replyerObj = $("#replyer");
											var replytextObj = $("#replyText");
											var id = replyerObj.val();
											var replytext = replytextObj.val();
											console.log(nno);
											event.preventDefault();
											$
													.ajax({
														type : 'post',
														url : '/nreplies/',
														headers : {
															"Content-Type" : "application/json",
															"X-HTTP-Method-Override" : "POST"
														},
														dataType : 'text',
														data : JSON
																.stringify({
																	nno : nno,
																	id : id,
																	replytext : replytext
																}),
														success : function(
																result) {
															console
																	.log("result: "
																			+ result);
															if (result == 'SUCCESS') {
																alert("등록 되었습니다.");
																replyPage = 1;
																getPage("/nreplies/"
																		+ nno
																		+ "/"
																		+ replyPage);
																/*replyerObj
																		.val(""); 아이디 초기화*/ 
																replytextObj
																		.val("");
															}
														}
													});
										});
				

						$("#modifyReplyBtn")
								.on(
										"click",
										function() {
											console.log("수정버튼 클림ㄱ");
											var nrno = $(".modal-title").html();
											var replytext = $("#replytext")
													.val();

											$
													.ajax({
														type : 'put',
														url : '/nreplies/'
																+ nrno,
														headers : {
															"Content-Type" : "application/json",
															"X-HTTP-Method-Override" : "PUT"
														},
														data : JSON
																.stringify({
																	replytext : replytext
																}),
														dataType : 'text',
														success : function(
																result) {
															console
																	.log("result: "
																			+ result);
															if (result == 'SUCCESS') {
																alert("수정 되었습니다.");
																getPage("/nreplies/"
																		+ nno
																		+ "/"
																		+ replyPage);
															}
														}
													});
										});

						$("#deleteReplyBtn")

								.on(
										"click",
										function() {

											var nrno = $(".modal-title").html();
											var replytext = $("#replytext")
													.val();

											$
													.ajax({
														type : 'delete',
														url : '/nreplies/'
																+ nrno,
														headers : {
															"Content-Type" : "application/json",
															"X-HTTP-Method-Override" : "DELETE"
														},
														dataType : 'text',
														success : function(
																result) {
															console
																	.log("result: "
																			+ result);
															if (result == 'SUCCESS') {
																alert("삭제 되었습니다.");
																getPage("/nreplies/"
																		+ nno
																		+ "/"
																		+ replyPage);
															}
														}
													});
										});
					});
</script>
<!-- /.content -->







<%@ include file="../include/footer.jsp"%>