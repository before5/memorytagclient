package com.before5.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.before5.domain.LoginVO;
import com.before5.domain.PageMaker;
import com.before5.domain.SearchCriteria;
import com.before5.service.OrderService;

@Controller
@RequestMapping("/order/*")
public class SearchOrderController {  //SearchOrderController

	@Inject
	private OrderService service; //orderService 주입
	
	//listPage 그대로 사용됨. 경로는 "/list"로 파라미터를 SearchCriteria로 변경함
//	@RequestMapping(value = "/list", method = RequestMethod.GET)
//	public void listPage(@ModelAttribute("cri") SearchCriteria cri, Model model) throws Exception {
//
//		System.out.println(service.listCountCriteria(cri));
//		
//		model.addAttribute("list", service.listCriteria(cri));
//		
//		PageMaker pageMaker = new PageMaker(); // pageMaker 생성
//		pageMaker.setCri(cri); // cri에는 page와 perPageNum이 있음
//		
//		pageMaker.setTotalCount(service.listCountCriteria(cri)); // 실제 totalcount를 입력받음
//																	
//		model.addAttribute("pageMaker", pageMaker); // pageMaker를 model 객체에 담음
//	}
	
	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public void listPage(@ModelAttribute("cri") SearchCriteria cri, 
						 Model model,LoginVO vo,HttpSession session) throws Exception {

		if(session.getAttribute("login") != null){
			Object user = session.getAttribute("login");	
			LoginVO login =  (LoginVO) user;			//LoginVO로 다운캐스팅해서 login에 담는다.
			int grade = login.getGrade(); // 로그인 한 등급을 가지고 온다.
			vo.setGrade(grade); // 등급을 셋팅해준다!!
			
			}
		
		
		System.out.println(service.listCountCriteria(cri));
		//System.out.println("hi");
		//model.addAttribute("list", service.listCriteria(cri));
		model.addAttribute("list", service.listSearchCriteria(cri));
		
		PageMaker pageMaker = new PageMaker(); // pageMaker 생성
		pageMaker.setCri(cri); // cri에는 page와 perPageNum이 있음
		// pageMaker.setTotalCount(28); //전에는 totalCount를 직접 입력해야했음

		//pageMaker.setTotalCount(service.listCountCriteria(cri)); 
		
		pageMaker.setTotalCount(service.listSearchCount(cri));

		model.addAttribute("pageMaker", pageMaker); // pageMaker를 model 객체에 담음
	}
	
	

	//게시물 삭제 처리
	
	
	//Integer[]를 String[] 로 바꾸니까됨.
	@RequestMapping(value = "/removePage" , method = RequestMethod.POST)
	public String remove(@RequestParam("ono") String[] ono, //string 배열 ono로 파라미터 전달함
			SearchCriteria cri,
			RedirectAttributes rttr) throws Exception {
		

		for(String ono2:ono){  
			service.remove(Integer.parseInt(ono2));  //ono2가 String 배열이므로 integer로 파싱함
			                                         //Integer[]도 가능함
		}
		
		rttr.addAttribute("page", cri.getPage());
		rttr.addAttribute("perPageNum", cri.getPerPageNum());
		rttr.addAttribute("searchType", cri.getSearchType());
		rttr.addAttribute("keyword", cri.getKeyword());
		
		rttr.addFlashAttribute("msg", "SUCCESS");
		
		
		return "redirect:/order/list";
		
	}
	
}
