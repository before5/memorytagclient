package com.before5.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.before5.domain.Criteria;
import com.before5.domain.LoginVO;
import com.before5.domain.NReplyVO;
import com.before5.domain.PageMaker;
import com.before5.service.NReplyService;

@RestController
@RequestMapping("/nreplies")
public class NReplyController {

	private static final Logger logger = LoggerFactory.getLogger(NReplyController.class);

	@Inject
	private NReplyService nservice;
	
	@RequestMapping(value="", method=RequestMethod.POST)
	public ResponseEntity<String> register(@RequestBody NReplyVO nvo){
		ResponseEntity<String> entity = null;
		try{
			nservice.addNReply(nvo);
			entity = new ResponseEntity<String>("SUCCESS", HttpStatus.OK); 
		}catch(Exception e){
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(), HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	@RequestMapping(value="/{nno}/{page}", method=RequestMethod.GET)
	public ResponseEntity<Map<String,Object>> listPage(@PathVariable("nno") Integer nno, @PathVariable("page") Integer page){
		ResponseEntity<Map<String,Object>> entity = null;
		try{
			Criteria cri = new Criteria();
			cri.setPage(page);

			System.out.println("nno: " + nno+", page: " + page);
			PageMaker pageMaker = new PageMaker();
			pageMaker.setCri(cri);
			
			Map<String, Object> map = new HashMap<>();
			List<NReplyVO> list = nservice.listNReplyPage(nno, cri);
			for(int i=0;i<list.size();i++)
			{System.out.println(list.get(i));}
			map.put("list", list);
			
			int replyCount = nservice.count(nno);
			pageMaker.setTotalCount(replyCount);
			System.out.println(replyCount);
			map.put("pageMaker", pageMaker);
			
			entity = new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK);
			
		}
		catch(Exception e){
			e.printStackTrace();
			entity = new ResponseEntity<Map<String,Object>>(HttpStatus.BAD_REQUEST);
		}
		
		return entity;
	}
	
	@RequestMapping(value = "/{nrno}", method={RequestMethod.PUT,RequestMethod.PATCH})
	public ResponseEntity<String> update(@PathVariable("nrno") Integer nrno,@RequestBody NReplyVO nvo){
		ResponseEntity<String> entity = null;
		try{
			nvo.setNrno(nrno);
			nservice.modifyNReply(nvo);
			entity = new ResponseEntity<String>("SUCCESS",HttpStatus.OK);
		}catch(Exception e){
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	
	@RequestMapping(value="/{nrno}", method=RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable("nrno") Integer nrno){
		ResponseEntity<String> entity = null;
		try{
			nservice.deleteNReply(nrno);
			entity = new ResponseEntity<String>("SUCCESS",HttpStatus.OK);
		}catch(Exception e){
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
}
