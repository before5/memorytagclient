package com.before5.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.before5.domain.Criteria;
import com.before5.domain.PageMaker;
import com.before5.domain.QReplyVO;
import com.before5.service.QReplyService;

@RestController
@RequestMapping("/replies")
public class ReplyController {
	
	private static final Logger logger = LoggerFactory.getLogger(ReplyController.class);
	
	@Inject 
	public QReplyService service; // dao에 접근하기 위해 사용하는 service
	
	//댓글을 등록하기 위한 메소드 
	@RequestMapping(value="/registReply" ,method=RequestMethod.POST)
	public ResponseEntity<String> registReply(@RequestBody QReplyVO vo){
		
		logger.info("들어왔을까나");
		ResponseEntity<String> entity = null;
		
		try{
			service.addReply(vo); 
			entity = new ResponseEntity<String>("SUCCESS",HttpStatus.BAD_REQUEST.OK); 
			// 성공했을 경우 string으로 success라는 메세지를 보낸다. 
		}catch(Exception e){
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST);
			// 실패했을 경우 메세지를 보낸다. 
		}
		return entity; // 메세지로 성공과 실패를 알 수 있다. 
	}
	//댓글 수정
	@RequestMapping(value="/{rno}", method={RequestMethod.PUT,RequestMethod.PATCH})
	public ResponseEntity<String> updateReply(@PathVariable("rno") Integer rno,@RequestBody QReplyVO vo){
		
		System.out.println("댓글 수정");
		System.out.println(rno);
		System.out.println(vo);
		ResponseEntity<String> entity = null;
		
		try {
			vo.setRno(rno); // rno를 셋팅해주고
			service.modifyReply(vo); // 수정할 vo를 넘겨준다.
			entity = new ResponseEntity<String>("SUCCESS",HttpStatus.BAD_REQUEST.OK); // 성공하면 성공 메세지
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST); //실패하면 실패 메세지 
		}
		
		return entity;
	}
	
	//댓글 삭제
	@RequestMapping(value="/{rno}", method=RequestMethod.DELETE)
	public ResponseEntity<String> deleteReply(@PathVariable("rno") Integer rno){
		
		ResponseEntity<String> entity = null;
		
		try {
			service.deleteReply(rno); // 댓글을 삭제하고
			entity = new ResponseEntity<String>("SUCCESS",HttpStatus.BAD_REQUEST.OK); //성공하면 성공 메세지
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			entity = new ResponseEntity<String>(e.getMessage(),HttpStatus.BAD_REQUEST); //실패하면 실패메세지
		}
		return entity;
	}
	
	//댓글을 페이지에 맞춰 가져오기 위한 메소드 
	@RequestMapping(value="/{qno}/{page}", method=RequestMethod.GET)
	public ResponseEntity<Map<String,Object>> replyPageList(@PathVariable("qno") Integer qno,@PathVariable("page") int page ){
		
		ResponseEntity<Map<String,Object>> entity = null;
		
		try{
			//criteria 설정 
			Criteria cri = new Criteria();
			cri.setPage(page); // page설정했다.
			int total = service.countReply(qno);
			System.out.println(total);
			//pageMaker 가져가야하는데 흠 
			PageMaker pageMaker = new PageMaker();
			pageMaker.setCri(cri);
			pageMaker.setTotalCount(total);
			logger.info(pageMaker.toString());
			//qno로 list가져오기
			List<QReplyVO> replyList = service.pageReply(cri, qno);
			
			Map<String,Object> map = new HashMap<String,Object>();
			
			map.put("pageMaker", pageMaker);
			
			map.put("replyList", replyList);
			
			entity = new ResponseEntity<Map<String,Object>>(map,HttpStatus.OK); // pageMaker와 replyList를 넣어서 전달한다. 
		}catch(Exception e){
			e.printStackTrace();
			entity = new ResponseEntity<Map<String,Object>>(HttpStatus.BAD_REQUEST);
		}
		return entity;
	}
	

}
