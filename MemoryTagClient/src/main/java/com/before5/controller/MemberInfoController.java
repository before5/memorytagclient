package com.before5.controller;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.before5.domain.LoginVO;
import com.before5.domain.MemberVO;
import com.before5.service.MemberInfoService;

@Controller
@RequestMapping("/mypage")
public class MemberInfoController {

	private static final Logger logger = LoggerFactory.getLogger(MemberInfoController.class);
	
	@Inject
	private MemberInfoService service;
	
	@RequestMapping(value="/memberInfo", method=RequestMethod.GET)
	public void memberInfoGET( Model model,HttpSession session, LoginVO vo){
		logger.info("memberInfo get........");
		Object user = session.getAttribute("login"); 
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.
		logger.info(id);
		model.addAttribute("id",id); // model에 넣어준다.
		model.addAttribute("vo", service.readMemberInfo(id));
		
		if(session.getAttribute("login") != null){
			
			int grade = login.getGrade(); // 로그인 한 등급을 가지고 온다.
			vo.setGrade(grade); // 등급을 셋팅해준다!!
			
			}
	}
	
	@RequestMapping(value="/memberInfo", method=RequestMethod.POST)
	public String memberInfoPOST(@RequestBody MemberVO vo) throws Exception{  //@RequestBody JSON데이터를 vo로 바꿔준다.
		System.out.println(vo);
		service.modifyMemberInfo(vo);
		
		
		
		return "redirect:/mypage/myorder";
	}
}
