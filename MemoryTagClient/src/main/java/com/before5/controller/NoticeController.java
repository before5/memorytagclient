package com.before5.controller;

import java.io.FileInputStream;
import java.io.InputStream;

import javax.annotation.Resource;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.before5.domain.LoginVO;
import com.before5.domain.NoticeFileVO;
import com.before5.domain.NoticeVO;
import com.before5.domain.PageMaker;
import com.before5.domain.SearchCriteria;
import com.before5.service.NReplyService;
import com.before5.service.NoticeService;
import com.before5.util.MediaUtils;
import com.before5.util.UploadFileUtils;

@Controller
@RequestMapping("/notice")
public class NoticeController {

	private static final Logger logger = LoggerFactory.getLogger(NoticeController.class);

	@Inject
	private NoticeService service;
	
	@Resource(name = "uploadPath")
	private String uploadPath;


	@RequestMapping(value="/register", method=RequestMethod.GET)
	public void registerGET (NoticeVO vo, Model model,HttpSession session) throws Exception{
		logger.info("register get........");
		
		Object user = session.getAttribute("login"); 
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.
		System.out.println(id);
		model.addAttribute("id",id); // model에 넣어준다. 
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String registerPOST (NoticeVO vo, @RequestParam("file") MultipartFile[] files,RedirectAttributes rttr) throws Exception{
		logger.info("register post........");
		logger.info(vo.toString());

		System.out.println(files.length+"ddddddddddddddddddd글씨");
		service.register(vo);
	
		rttr.addFlashAttribute("result", "공지가 등록되었습니다.");
		//List<String> temp = new ArrayList<>();
		for (MultipartFile file : files) {
			//UUID uid = UUID.randomUUID();
			//String savedName = uid.toString() +"_"+file.getOriginalFilename();
			
			NoticeFileVO filevo = new NoticeFileVO();
			filevo.setStored_file_name(UploadFileUtils.uploadFile(uploadPath, file.getOriginalFilename(), file.getBytes()));
			filevo.setOriginal_file_name(file.getOriginalFilename());
			filevo.setExtension(file.getContentType());
			filevo.setFiledirectory(UploadFileUtils.calcPath(uploadPath));
			
			filevo.setFile_size(file.getSize());
			System.out.println("=================+++++++++++++++"+filevo);
			service.addAttach(filevo);
			
			//temp.add(savedName);
			//File target = new File("C:\\memorytag\\notice\\"+savedName);
//			try{
//				FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(target));
//			}catch(Exception e){
//				e.printStackTrace();
//			}
			
		}
//		if(filevo.getOriginal_file_name() !=null){
//			try{
//				//service
//			}catch(Exception e){
//				e.printStackTrace();
//			}
//		}
		return "redirect:/notice/listPage";
	}
	
//	@RequestMapping(value="/listAll", method=RequestMethod.GET)
//	public void listAll (@ModelAttribute("cri")Criteria cri,Model model) throws Exception{
//		logger.info("show all list........");
//		model.addAttribute("list", service.listAll());
//		PageMaker pagemaker = new PageMaker();
//		pagemaker.setCri(cri);
//		logger.info("show notice list........");
//		model.addAttribute("pagemaker", pagemaker );
//	}
	
//	@RequestMapping(value="/read", method=RequestMethod.GET)
//	public void read (@RequestParam("nno") int nno,Model model) throws Exception{
//		logger.info("read........");
//		model.addAttribute("notice", service.read(nno));
//		logger.info("show notice list........");
//	}
	
	@RequestMapping(value="/readPage", method=RequestMethod.GET)
	public void read (@RequestParam("nno") @PathVariable("nno") Integer nno,@ModelAttribute("cri") SearchCriteria cri, 
            Model model,LoginVO vo, HttpSession session) throws Exception{
		logger.info("read........");
		
		if(session.getAttribute("login") != null){
			Object user = session.getAttribute("login");	
			LoginVO login =  (LoginVO) user;			//LoginVO로 다운캐스팅해서 login에 담는다.
			int grade = login.getGrade(); // 로그인 한 등급을 가지고 온다.
			vo.setGrade(grade); // 등급을 셋팅해준다!!
			String id = login.getId();
			model.addAttribute("id",id);
			}
		

		model.addAttribute("notice", service.read(nno));
		model.addAttribute("attach", service.getAttach(nno));
		service.plusViewCnt(nno);
		
		logger.info("show notice list........");
	}
	
	@ResponseBody
	@RequestMapping("/displayFile")
	public ResponseEntity<byte[]> displayFile(String fileName) throws Exception{
		
		InputStream in=null;
		ResponseEntity<byte[]> entity = null;
		logger.info("FILE NAME : "+ fileName);
		
		try{
			String formatName = fileName.substring(fileName.lastIndexOf(".")+1);
			
			MediaType mType = MediaUtils.getMediaType(formatName);
			
			HttpHeaders headers = new HttpHeaders();
			in = new FileInputStream(uploadPath+fileName);
			
			if(mType != null){
				headers.setContentType(mType);
			}else{
				fileName = fileName.substring(fileName.indexOf("_")+1);
				headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
				headers.add("Content-Disposition", "attachment; filename=\""+
				  new String(fileName.getBytes("UTF-8"),"ISO-8859-1")+"\"");
			}
			
			entity = new ResponseEntity<byte[]>(IOUtils.toByteArray(in),headers,HttpStatus.CREATED);
		}catch(Exception e){e.printStackTrace();
							entity = new ResponseEntity<byte[]>(HttpStatus.BAD_REQUEST);
		}finally{in.close();}
		return entity;
	}
	
	@RequestMapping(value="/deletePage", method=RequestMethod.POST)
	public String delete (@RequestParam("nno") int nno, SearchCriteria cri,RedirectAttributes rttr) throws Exception{
		logger.info("register post........");
		System.out.println(nno);
		service.delete(nno);
		rttr.addAttribute("page", cri.getPage());
		rttr.addAttribute("perPageNum", cri.getPerPageNum());
		rttr.addAttribute("searchType", cri.getSearchType());
		rttr.addAttribute("keyword", cri.getKeyword());
		rttr.addFlashAttribute("result", "������ �����Ǿ����ϴ�.");
		
		return "redirect:/notice/listPage";
	}
	
	@RequestMapping(value="/modifyPage", method=RequestMethod.GET)
	public void modifyPagingGET (@RequestParam("nno")int nno, @ModelAttribute("cri") SearchCriteria cri,Model model) throws Exception{
		logger.info("modify get........");
		model.addAttribute("notice",service.read(nno));
		logger.info(service.read(nno).toString());
	}
	
	@RequestMapping(value="/modifyPage", method=RequestMethod.POST)
	public String modifyPOST (NoticeVO vo,SearchCriteria cri,RedirectAttributes rttr, @RequestParam("file") MultipartFile[] files) throws Exception{
		logger.info("modify post........");
		System.out.println(vo+"222222222222");
		service.modify(vo);
		for (MultipartFile file : files) {
			//UUID uid = UUID.randomUUID();
			//String savedName = uid.toString() +"_"+file.getOriginalFilename();
			
			NoticeFileVO filevo = new NoticeFileVO();
			filevo.setNno(vo.getNno());
			filevo.setStored_file_name(UploadFileUtils.uploadFile(uploadPath, file.getOriginalFilename(), file.getBytes()));
			filevo.setOriginal_file_name(file.getOriginalFilename());
			filevo.setExtension(file.getContentType());
			filevo.setFiledirectory(UploadFileUtils.calcPath(uploadPath));
			System.out.println(vo.getNno()+"nno");
			filevo.setFile_size(file.getSize());
			System.out.println("=================+++++++++++++++"+filevo);
			service.addAttach2(filevo);
			
			//temp.add(savedName);
			//File target = new File("C:\\memorytag\\notice\\"+savedName);
//			try{
//				FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(target));
//			}catch(Exception e){
//				e.printStackTrace();
//			}
			
		}
		rttr.addAttribute("page", cri.getPage());
		rttr.addAttribute("perPageNum", cri.getPerPageNum());
		rttr.addAttribute("searchType", cri.getSearchType());
		rttr.addAttribute("keyword", cri.getKeyword());
		rttr.addFlashAttribute("result", "������ �����Ǿ����ϴ�.");
		logger.info(rttr.toString());
		return "redirect:/notice/listPage";
	}
	
	@RequestMapping(value="/listPage", method=RequestMethod.GET)
	public void listCriteria (@ModelAttribute("cri") SearchCriteria cri,Model model,LoginVO vo, HttpSession session) throws Exception{
		if(session.getAttribute("login") != null){
			Object user = session.getAttribute("login");	
			LoginVO login =  (LoginVO) user;			//LoginVO로 다운캐스팅해서 login에 담는다.
			int grade = login.getGrade(); // 로그인 한 등급을 가지고 온다.
			vo.setGrade(grade); // 등급을 셋팅해준다!!
			
			}
		
		System.out.println("----------------------------------------------");
		logger.info(cri.toString());
		//model.addAttribute("list", service.listCriteria(cri));
		model.addAttribute("list", service.listSearchCriteria(cri));
	
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
//		pagemaker.setTotalCount(131);
//		pagemaker.setTotalCount(service.listCountCriteria(cri));
		pageMaker.setTotalCount(service.listSearchCount(cri));
		System.out.println(pageMaker);
		logger.info("show notice list........");
		model.addAttribute("pagemaker", pageMaker );
	}
}

