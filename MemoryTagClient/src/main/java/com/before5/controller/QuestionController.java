package com.before5.controller;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.before5.domain.LoginVO;
import com.before5.domain.PageMaker;
import com.before5.domain.QuestionVO;
import com.before5.domain.SearchCriteria;
import com.before5.service.QuestionService;

@Controller
@RequestMapping("/question/*")
public class QuestionController {
	
	private static final Logger logger = LoggerFactory.getLogger(QuestionController.class);
	
	@Inject
	public QuestionService service;  // service 객체
	
	
/*	@RequestMapping(value ="/list", method=RequestMethod.GET)
	public void listGET(@ModelAttribute("cri") Criteria cri,Model model) throws Exception{
		
		logger.info(cri.toString()); 
		
		model.addAttribute("list",service.pageList(cri)); // 현재 page를 가지고 있는 cri로 현재 page에 맞는 data를 가져올꺼다.
		
		PageMaker pageMaker = new PageMaker(); // pageMaker을 넘겨 주어야 하므로 pageMaker객체 생성
		pageMaker.setCri(cri); // pageMaker의 cri를 셋팅 (현재 페이지를 넘겨준다)
		pageMaker.setTotalCount(service.getTotalCount());// 전체 데이터수를 넘겨서 pageMaker를 setting!!
		
		logger.info(pageMaker.toString());
		model.addAttribute("pageMaker",pageMaker); // jsp파일에 pageMaker를 보내준다.
		
	} // 모든 공지사항 list를 보여줄 method
*/	
	@RequestMapping(value="/read", method=RequestMethod.GET)
	public void readPage(@RequestParam("qno") int qno,@ModelAttribute("cri") SearchCriteria cri, Model model,HttpSession session) throws Exception{
		
		logger.info("read Q&A.......");
		Object user = session.getAttribute("login"); 
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.
		System.out.println(id);
		model.addAttribute("id",id); // model에 넣어준다. 
		
		model.addAttribute(service.readOneQuestion(qno));
		
	} // 하나의 공지사항을 보여줄 method
	
	@RequestMapping(value="/delete", method=RequestMethod.POST)
	public String removePOST(@ModelAttribute("cri") SearchCriteria cri, QuestionVO vo, Model model) throws Exception{
		logger.info("remove post");
		System.out.println("들어왔남");
		System.out.println(vo.toString()); 
		service.deleteQuestion(vo.getQno());
		service.deleteReply(vo.getQno()); //오오 같이 지우는구만 
		
		return "redirect:/question/list?page="+cri.getPage();
	}
	
	@RequestMapping(value="/register", method=RequestMethod.GET)
	public void registerGet(Model model,HttpSession session) throws Exception{
		logger.info("question register get");
		
		Object user = session.getAttribute("login"); 
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.
		System.out.println(id);
		
		model.addAttribute("id",id);
		
	}
	
	@RequestMapping(value="/register", method=RequestMethod.POST)
	public String registerPost(QuestionVO vo, Model model) throws Exception{
		logger.info("question register post");
		
		System.out.println(vo);
		service.insertQuestion(vo);
		
		return "redirect:/question/list";
		
	}
	@RequestMapping(value="/modify", method=RequestMethod.GET)
	public void modifyGet(int qno, @ModelAttribute("cri") SearchCriteria cri, Model model,HttpSession session) throws Exception{
		logger.info("question modify");
		
		Object user = session.getAttribute("login"); 
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.
		
		System.out.println(id);
		QuestionVO vo = service.readOneQuestion(qno);
	
		model.addAttribute("id",id);
		model.addAttribute("questionVO",vo);
		
	}
	@RequestMapping(value="/modify",method=RequestMethod.POST)
	public String modiftPost(QuestionVO vo) throws Exception{
		logger.info("question modify post");
		
		System.out.println(vo);
		service.updateQuestion(vo);
		return "redirect:/question/list";
	}
	@RequestMapping(value="/list", method= RequestMethod.GET)
	public void list2Get(@ModelAttribute("cri") SearchCriteria cri, Model model,LoginVO vo,HttpSession session) throws Exception{
		logger.info(cri.toString());
		
		if(session.getAttribute("login") != null){
			Object user = session.getAttribute("login");	
			LoginVO login =  (LoginVO) user;			//LoginVO로 다운캐스팅해서 login에 담는다.
			int grade = login.getGrade(); // 로그인 한 등급을 가지고 온다.
			vo.setGrade(grade); // 등급을 셋팅해준다!!
			
			}
		
		//model.addAttribute("list",service.listCriteria(cri));
		
		//model.addAttribute("list",service.listSearch(cri));//검색했을 때 넣는거어 
		
		model.addAttribute("list",service.listSearchReply(cri));
		List<QuestionVO> list = service.listSearchReply(cri);
		for (QuestionVO questionVO : list) {
			System.out.println(questionVO);
		}
		
		PageMaker pageMaker = new PageMaker();
		pageMaker.setCri(cri);
		
	    //pageMaker.setTotalCount(service.listCountCriteria(cri));
		pageMaker.setTotalCount(service.listSearchCount(cri));
		model.addAttribute("pageMaker",pageMaker);
	}
	
	@RequestMapping(value="/addReply", method=RequestMethod.GET)
	public void addReplyGet(QuestionVO vo,@ModelAttribute("cri") SearchCriteria cri,Model model,HttpSession session) throws Exception{
		logger.info("답글달기 get에 들어왔습니다.");

		Object user = session.getAttribute("login"); 
		LoginVO login = (LoginVO) user;
		String id = login.getId(); // 로그인 한 아이디를 가지고 온다.
		
		System.out.println(id);
		model.addAttribute("id",id);
		model.addAttribute("cri",cri);		
			
	}
	
	@RequestMapping(value="/addReply", method=RequestMethod.POST)
	public String addReply(QuestionVO vo,@ModelAttribute("cri") SearchCriteria cri,Model model) throws Exception{
		
		logger.info("답글달기 post에 들어왔습니다.");
		
		//family는 원글과 같게 , parent는 원글의 qno, depth는 원글 +1, indent는 원글 +1
		int parent = vo.getQno();
		int depth = vo.getDepth()+1;
		int indent = vo.getIndent()+1;
		
		vo.setParent(parent); //vo에 parent 셋팅
		vo.setDepth(depth); //vo에 depth셋팅
		vo.setIndent(indent); //vo에 indent셋팅
		
		System.out.println("답글의 vo: "+vo);
		
		//여기서부터 insert작업 
		service.insertReply(vo);
		
		//list페이지로
		return "redirect:/question/list?page="+cri.getPage();
				
	}
	@RequestMapping(value="/checkParent/{qno}",method=RequestMethod.GET)
	public ResponseEntity<String> checkParent(@PathVariable("qno") int qno) throws Exception{
		
		//qno로 parent가 있는지 판별 
		if(service.checkParent(qno) ==0){
			//자식이 없다 // 삭제해도 된다.
			return new ResponseEntity<String>("Noparent",HttpStatus.OK);
		}
		//자식이 있다는 소리이다.
		return new ResponseEntity<String>("parent",HttpStatus.OK);
	}
	
}
