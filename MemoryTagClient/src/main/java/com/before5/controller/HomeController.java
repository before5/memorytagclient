package com.before5.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.before5.domain.LoginVO;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model,LoginVO vo,HttpSession session) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		if(session.getAttribute("login") != null){
		Object user = session.getAttribute("login");	
		LoginVO login =  (LoginVO) user;			//LoginVO로 다운캐스팅해서 login에 담는다.
		int grade = login.getGrade(); // 로그인 한 등급을 가지고 온다.
		vo.setGrade(grade); // 등급을 셋팅해준다!!
		
		}
		
		return "home3";
	}
	
	@RequestMapping(value="/test", method = RequestMethod.GET)
	public void ajaxTest(){
		
	}
	
}
