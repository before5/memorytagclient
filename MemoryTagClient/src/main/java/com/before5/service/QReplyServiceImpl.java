package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.before5.domain.Criteria;
import com.before5.domain.QReplyVO;
import com.before5.persistence.QReplyDAO;
import com.before5.persistence.QuestionDAO;

@Service
public class QReplyServiceImpl implements QReplyService{

	@Inject 
	public QReplyDAO dao;
	
	@Inject 
	public QuestionDAO qDao;

	@Override
	public List<QReplyVO> allReply(int qno) throws Exception {
		// TODO Auto-generated method stub
		return dao.allReply(qno);
	}

	@Transactional
	@Override
	public void addReply(QReplyVO vo) throws Exception{
		// TODO Auto-generated method stub
		dao.addReply(vo);
		qDao.updateReplyCnt(vo.getQno(), 1);
	}

	@Override
	public void modifyReply(QReplyVO vo) throws Exception{
		// TODO Auto-generated method stub
		dao.modifyReply(vo);
	}

	@Transactional
	@Override
	public void deleteReply(int rno) throws Exception{
		// TODO Auto-generated method stub
		int qno = dao.getQno(rno);
		dao.deleteReply(rno);
		System.out.println("qno : "+qno);
		qDao.updateReplyCnt(qno, -1);
	}

	@Override
	public int countReply(int qno) throws Exception{
		// TODO Auto-generated method stub
		return dao.countReply(qno);
	}

	@Override
	public List<QReplyVO> pageReply(Criteria cri, int qno) throws Exception {
		// TODO Auto-generated method stub
		return dao.pageReply(cri, qno);
	}
}
