package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.aop.ThrowsAdvice;
import org.springframework.stereotype.Service;

import com.before5.domain.Criteria;
import com.before5.domain.MemberVO;
import com.before5.domain.SearchCriteria;
import com.before5.persistence.MemberDAO;

@Service
public class MemberServiceImpl implements MemberService {
	
	@Inject // spring에서 dao를 사용할 수 있게 하기 위한 annotation
	public MemberDAO dao; // dao와 서비스를 연결하기 위해 필요한 dao

	@Override
	public void regist(MemberVO vo) throws Exception {
		dao.insertMember(vo); // 회원등록을 하기 위해 dao의 insertmember method를 이용하였다.
	}

	@Override
	public MemberVO readMember(String id) throws Exception {
		return dao.readMember(id); // 특정 id로 회원을 조회하기 위해 dao의 readMember
									// method이용
	}

	@Override
	public List<MemberVO> readAllMember() {
		return dao.selectAll(); // 모든 회원 목록을 조회하기 위해 dao의 selectAll method를
								// 이용하였다.
	}

	@Override
	public void updateMember(MemberVO vo) throws Exception {
		dao.updateMember(vo); // 회원정보를 수정하기 위해 dao의 updateMember method를 이용하였다.
	}

	@Override
	public void deleteMember(String id) throws Exception {
		dao.deleteMember(id); // 회원의 id로 회원의 정보를 삭제하기 위해 dao의 deleteMember
								// method를 이용하였다.
	}
	
	@Override
	public List<MemberVO> listCriteria(Criteria cri) throws Exception {
		
		return dao.listCriteria(cri);
	}

	@Override
	public int countPaging(Criteria cri) throws Exception {
		
		return dao.countPaging(cri);
	}
	@Override
	public List<MemberVO> listSearchCriteria(SearchCriteria cri) throws Exception {
		
		return dao.listSearch(cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		
		return dao.listSearchCount(cri);
	}
}
