package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.before5.domain.Criteria;
import com.before5.domain.NoticeFileVO;
import com.before5.domain.NoticeVO;
import com.before5.domain.SearchCriteria;
import com.before5.persistence.NoticeDAO;
@Service
public class NoticeServiceImpl implements NoticeService {

	@Inject
	private NoticeDAO dao;
	
	@Transactional
	@Override
	public void register(NoticeVO vo) throws Exception {
		dao.create(vo);
		
		
		
	}
	
	@Override
	public void addAttach(NoticeFileVO filevo) throws Exception {

			dao.addAttach(filevo);
			System.out.println(filevo);
	}
	
	@Override
	public NoticeVO read(Integer nno) throws Exception {
		
		return dao.read(nno);
	}

	@Override
	public void modify(NoticeVO vo) throws Exception {
		dao.update(vo);

	}

	@Override
	public void delete(Integer nno) throws Exception {
		dao.delete(nno);

	}

//	@Override
//	public List<NoticeVO> listAll() throws Exception {
//		
//		return dao.listAll();
//	}

//	@Override
//	public List<NoticeVO> listPage(Criteria cri) throws Exception {
//		// TODO Auto-generated method stub
//		return dao.listPage(page);
//	}

	
	@Override
	public List<NoticeVO> listCriteria(Criteria cri) throws Exception {
		
		return dao.listCriteria(cri);
	}

	@Override
	public int listCountCriteria(Criteria cri) throws Exception {
		
		return dao.countPaging(cri);
	}

	@Override
	public List<NoticeVO> listSearchCriteria(SearchCriteria cri) throws Exception {
		
		return dao.listSearch(cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		
		return dao.listSearchCount(cri);
	}

	@Override
	public List<String> getAttach(Integer nno) throws Exception {
		
		return dao.getAttach(nno);
	}

	@Override
	public void plusViewCnt(Integer nno) {
		// TODO Auto-generated method stub
		dao.updateViewCnt(nno);
	}

	@Override
	public void addAttach2(NoticeFileVO filevo) throws Exception {
		// TODO Auto-generated method stub
		dao.addAttach2(filevo);
	}

//	@Override
//	public void modifyAttach(NoticeFileVO filevo) throws Exception {
//		dao.updateAttach(filevo);
//		
//	}




}
