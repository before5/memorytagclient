package com.before5.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.ReservationVO;
import com.before5.persistence.ReservationDAO;

@Service
public class ReservationServiceImpl implements ReservationService {

	@Inject 
	ReservationDAO dao;
	
	@Override
	public void registerContent(ReservationVO vo) throws Exception{
		dao.registerContent(vo);
	}

	@Override
	public void registerFile(ReservationVO vo) throws Exception{
		dao.registerFile(vo);
	}

	@Override
	public void registerOrder(String id, int dno,String pw) throws Exception{
		dao.registerOrder(id, dno,pw);
	}

}
