package com.before5.service;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.MemberVO;
import com.before5.domain.SearchCriteria;

public interface MemberService {
	public void regist(MemberVO vo) throws Exception; // 처음 회원가입을 할 때 member를
														// 등록한다.

	public MemberVO readMember(String id) throws Exception; // id로 회원을 조회하여 해당
															// id를 가진 회원을 조회한다.

	public List<MemberVO> readAllMember() ; // 모든 회원 목록을 조회한다.

	public void updateMember(MemberVO vo) throws Exception; // 회원의 정보를 수정한다.

	public void deleteMember(String id) throws Exception; // 회원정보를 삭제한다.
	
	public List<MemberVO> listCriteria(Criteria cri)throws Exception;
	
	public int countPaging(Criteria cri)throws Exception;
	
	public List<MemberVO> listSearchCriteria(SearchCriteria cri)throws Exception;
	
	public int listSearchCount(SearchCriteria cri)throws Exception;
}
