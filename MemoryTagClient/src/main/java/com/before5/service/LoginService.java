package com.before5.service;

import java.util.Date;

import com.before5.domain.LoginVO;

public interface LoginService {		//로그인 서비스 인터페이스를 만든다.
			//서비스는 DAO와 Controller사이에서 와리가리하는 놈이다.
	
	public LoginVO login(LoginVO vo)throws Exception;	// 메소드를 만든다.
	
	public void keepLogin(String id, String sessionId, Date next)throws Exception;//세션값을 만드는 메소드
	
	public LoginVO checkLoginWithSessionKey(String value);//세션값을 확인하는 메소드
}
