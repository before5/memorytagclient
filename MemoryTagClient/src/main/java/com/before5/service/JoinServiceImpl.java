package com.before5.service;



import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.MemberVO;
import com.before5.persistence.JoinDAO;

@Service
public class JoinServiceImpl implements JoinService {

	@Inject
	public JoinDAO dao;
	@Override
	public void joinMember(MemberVO vo) throws Exception{
		// TODO Auto-generated method stub
		dao.joinMember(vo);
	}
	
	@Override
	public MemberVO checkId(String id) throws Exception {
		// TODO Auto-generated method stub
		return dao.searchId(id);
	}

}
