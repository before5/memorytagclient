package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.Criteria;
import com.before5.domain.NReplyVO;
import com.before5.persistence.NReplyDAO;
@Service
public class NReplyServiceImpl implements NReplyService {

	@Inject
	private NReplyDAO dao;
	
	@Override
	public void addNReply(NReplyVO nvo) throws Exception {
		// TODO Auto-generated method stub
		dao.create(nvo);
	}


	@Override
	public void modifyNReply(NReplyVO nvo) throws Exception {
		// TODO Auto-generated method stub
		dao.update(nvo);
	}

	@Override
	public void deleteNReply(Integer nrno) throws Exception {
		// TODO Auto-generated method stub
		dao.delete(nrno);
	}

	@Override
	public List<NReplyVO> listNReplyPage(Integer nno, Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return dao.listPage(nno, cri);
	}

	@Override
	public int count(Integer nno) throws Exception {
		// TODO Auto-generated method stub
		return dao.count(nno);
	}

}
