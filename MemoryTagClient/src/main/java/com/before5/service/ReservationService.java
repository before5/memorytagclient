package com.before5.service;

import com.before5.domain.ReservationVO;

public interface ReservationService {

	//예약 페이지 service
	//tbl_content 등록
	public void registerContent(ReservationVO vo) throws Exception;
	
	//tbl_file 등록
	public void registerFile(ReservationVO vo) throws Exception;
	
	//tbl_order 등록
	public void registerOrder(String id, int dno,String pw) throws Exception;
	
}
