package com.before5.service;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.MemberVO;

import com.before5.persistence.MemberInfoDAO;
@Service
public class MemberInfoServiceImpl implements MemberInfoService {

	@Inject
	private MemberInfoDAO dao;
	
	@Override
	public MemberVO readMemberInfo(String id) {
		// TODO Auto-generated method stub
		return dao.selectMemberInfo(id);
	}

	@Override
	public void modifyMemberInfo(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		dao.updateMemberInfo(vo);
	}

}
