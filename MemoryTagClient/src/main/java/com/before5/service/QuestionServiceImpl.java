package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.Criteria;
import com.before5.domain.QuestionVO;
import com.before5.domain.SearchCriteria;
import com.before5.persistence.QuestionDAO;

@Service
public class QuestionServiceImpl implements QuestionService {

	@Inject 
	QuestionDAO dao; // dao 객체 
	
	@Override
	public List<QuestionVO> readAllQuestion() throws Exception {
		// TODO Auto-generated method stub
		return dao.readAllQuestion();
	}

	@Override
	public QuestionVO readOneQuestion(Integer qno) throws Exception {
		// TODO Auto-generated method stub
		return dao.readOneQuestion(qno);
	}

	@Override
	public void deleteQuestion(Integer qno) throws Exception{
		// TODO Auto-generated method stub
		dao.deleteQuestion(qno);
	}

	@Override
	public int getTotalCount()throws Exception {
		// TODO Auto-generated method stub
		return dao.totalCount();
	}

	@Override
	public List<QuestionVO> pageList(Criteria cri) throws Exception {
		// TODO Auto-generated method stub
		return dao.pageList(cri);
	}

	@Override
	public void insertQuestion(QuestionVO vo) throws Exception {
		// TODO Auto-generated method stub
		dao.insertQuestion(vo);
	}

	@Override
	public void updateQuestion(QuestionVO vo) throws Exception {
		// TODO Auto-generated method stub
		dao.updateQuestion(vo);
	}

	@Override
	public void deleteReply(int qno) throws Exception {
		// TODO Auto-generated method stub
		dao.deleteReply(qno);
	}

	@Override
	public List<QuestionVO> listSearch(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return dao.listSearch(cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return dao.listSearchCount(cri);
	}

	@Override
	public void insertReply(QuestionVO vo) throws Exception {
		// TODO Auto-generated method stub
		dao.insertRply(vo);
	}

	@Override
	public List<QuestionVO> listSearchReply(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return dao.listSearchReply(cri);
	}

	@Override
	public int checkParent(int qno) throws Exception {
		// TODO Auto-generated method stub
		return dao.checkParent(qno);
	}
	
}
