package com.before5.service;

import org.springframework.stereotype.Service;

import com.before5.domain.MemberVO;


public interface MemberInfoService{

	public MemberVO readMemberInfo(String id);
	public void modifyMemberInfo(MemberVO vo) throws Exception;
}
