package com.before5.service;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.NReplyVO;

public interface NReplyService {

	public void addNReply(NReplyVO nvo) throws Exception;
	public List<NReplyVO> listNReplyPage(Integer nno, Criteria cri) throws Exception;
	public void modifyNReply(NReplyVO nvo) throws Exception;
	public void deleteNReply(Integer nrno) throws Exception;
	public int count(Integer nno) throws Exception;
}
