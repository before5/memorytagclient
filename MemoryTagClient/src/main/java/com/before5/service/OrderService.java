package com.before5.service;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.OrderVO;
import com.before5.domain.SearchCriteria;

public interface OrderService {
	

	public void remove(Integer ono) throws Exception;  //삭제 
	
	public List<OrderVO> listAll() throws Exception; // 전체목록 조회
	
	public List<OrderVO> listCriteria(Criteria cri) throws Exception;  // paging 처리
	
	public int listCountCriteria(Criteria cri) throws Exception;  // 실제 개수를 이용한 Count paging

	public List<OrderVO> listSearchCriteria(SearchCriteria cri) 
	
	throws Exception;  
	
	public int listSearchCount(SearchCriteria cri) throws Exception;
}
