package com.before5.service;

import java.util.List;

import javax.inject.Inject;

import org.springframework.stereotype.Service;

import com.before5.domain.Criteria;
import com.before5.domain.OrderVO;
import com.before5.domain.SearchCriteria;
import com.before5.persistence.OrderDAO;

@Service
public class OrderServiceImpl implements OrderService {
	
	@Inject
	private OrderDAO dao;

	@Override
	public void remove(Integer ono) throws Exception {
		
		dao.delete(ono);
		
	}

	@Override
	public List<OrderVO> listAll() throws Exception {
	
		return dao.listAll();
	}

	@Override
	public List<OrderVO> listCriteria(Criteria cri) throws Exception {
		
		return dao.listCriteria(cri);  //cri에는 page와 perPageNum이 들어있음
	}


	@Override
	public int listCountCriteria(Criteria cri) throws Exception {
		
		return dao.countPaging(cri);
	}

	@Override
	public List<OrderVO> listSearchCriteria(SearchCriteria cri) throws Exception {

		return dao.listSearch(cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		
		return dao.listSearchCount(cri);
		
	}

}
