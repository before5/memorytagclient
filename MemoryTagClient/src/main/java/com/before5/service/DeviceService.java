package com.before5.service;

import java.util.List;

import com.before5.domain.DeviceVO;

public interface DeviceService {
	
	public List<DeviceVO> listDevice() throws Exception;

}
