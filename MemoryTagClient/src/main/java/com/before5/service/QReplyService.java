package com.before5.service;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.QReplyVO;

// 문의사항 댓글 서비스
public interface QReplyService {
	
	//qno에 맞는 모든 댓글 가져오기
	public List<QReplyVO> allReply(int qno) throws Exception;
	
	//댓글 등록
	public void addReply(QReplyVO vo) throws Exception;
	
	//댓글 수정
	public void modifyReply(QReplyVO vo) throws Exception;
	
	//댓글 삭제
	public void deleteReply(int rno) throws Exception;
	
	//qno에 달려있는 모든 댓글 개수 세기
	public int countReply(int qno) throws Exception;
	
	//댓글 페이지에 맞는 댓글 리스트 가져오기
	public List<QReplyVO> pageReply(Criteria cri,int qno) throws Exception;
}
