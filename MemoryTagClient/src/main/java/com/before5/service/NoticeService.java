package com.before5.service;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.NoticeFileVO;
import com.before5.domain.NoticeVO;
import com.before5.domain.SearchCriteria;

public interface NoticeService {

	public void register (NoticeVO vo) throws Exception;
	public void addAttach(NoticeFileVO filevo) throws Exception;
	public void addAttach2(NoticeFileVO filevo) throws Exception;
	//public void modifyAttach(NoticeFileVO filevo) throws Exception;
	public NoticeVO read (Integer nno) throws Exception;
	public void modify (NoticeVO vo) throws Exception;
	public void delete (Integer nno) throws Exception;
	//public List<NoticeVO> listAll () throws Exception;
	//public List<NoticeVO> listPage (int page) throws Exception;
	public List<NoticeVO> listCriteria (Criteria cri) throws Exception;
	public int listCountCriteria(Criteria cri) throws Exception;
	public List<NoticeVO> listSearchCriteria (SearchCriteria cri) throws Exception;
	public int listSearchCount(SearchCriteria cri) throws Exception;
	public List<String> getAttach(Integer nno) throws Exception;
	public void plusViewCnt(Integer nno);
}
