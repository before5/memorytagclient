package com.before5.domain;

public class FileVO {

	private int fno;
	private int cno;
	private String filename;
	private String type;
	private String directory;
	private int f_size;
	private String path;
	private String extension;
	private String thumnail;
	private String original;
	
	
	
	
	public String getOriginal() {
		return original;
	}
	public void setOriginal(String original) {
		this.original = original;
	}
	public String getThumnail() {
		return thumnail;
	}
	public void setThumnail(String thumnail) {
		this.thumnail = thumnail;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getFno() {
		return fno;
	}
	public void setFno(int fno) {
		this.fno = fno;
	}
	public int getCno() {
		return cno;
	}
	public void setCno(int cno) {
		this.cno = cno;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	public int getF_size() {
		return f_size;
	}
	public void setF_size(int f_size) {
		this.f_size = f_size;
	}
	@Override
	public String toString() {
		return "FileVO [fno=" + fno + ", cno=" + cno + ", filename=" + filename + ", type=" + type + ", directory="
				+ directory + ", f_size=" + f_size + ", path=" + path + ", extension=" + extension + ", thumnail="
				+ thumnail + ", original=" + original + "]";
	}
	
	
}
