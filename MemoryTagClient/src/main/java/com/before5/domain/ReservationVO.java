package com.before5.domain;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class ReservationVO {

	// 주문 테이블 
	private Integer ono;
	private String id;
	private Integer[] dnos;  // dno는 여러개 가질 수 있다. dno 개수대로 
	private Integer cno;
	private String text;
	private String pw;
	//tbl_order에 들어갈 것
	
	private String title;
	private Date regdate;
	private Date orderdate;
	private int fontsize;
	private String fontcolor;
	// tbl_content에 들어갈 것 
	
	private Integer fno;
	private String fileName;
	private String directory;
	private Integer size;
	private String type;
	private String extension;
	// tbl_file에 들어갈 것
	
	
	
	
	public Integer getOno() {
		return ono;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public int getFontsize() {
		return fontsize;
	}
	public void setFontsize(int fontsize) {
		this.fontsize = fontsize;
	}
	public String getFontcolor() {
		return fontcolor;
	}
	public void setFontcolor(String fontcolor) {
		this.fontcolor = fontcolor;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	public Integer getSize() {
		return size;
	}
	public void setSize(Integer size) {
		this.size = size;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public void setOno(Integer ono) {
		this.ono = ono;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	
	public Integer[] getDnos() {
		return dnos;
	}
	public void setDnos(Integer[] dnos) {
		this.dnos = dnos;
	}
	public Integer getCno() {
		return cno;
	}
	public void setCno(Integer cno) {
		this.cno = cno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(String orderdate) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHH");
		Date tempDate=null;
		try {
			tempDate = format.parse(orderdate);			//들어온 스트링값을 변환한다.
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.out.println(tempDate.toString());
		
		this.orderdate=tempDate;		
		System.out.println(tempDate);
	}
	public Integer getFno() {
		return fno;
	}
	public void setFno(Integer fno) {
		this.fno = fno;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public String toString() {
		return "ReservationVO [ono=" + ono + ", id=" + id + ", dnos=" + Arrays.toString(dnos) + ", cno=" + cno
				+ ", text=" + text + ", pw=" + pw + ", title=" + title + ", regdate=" + regdate + ", orderdate="
				+ orderdate + ", fontsize=" + fontsize + ", fontcolor=" + fontcolor + ", fno=" + fno + ", fileName="
				+ fileName + ", directory=" + directory + ", size=" + size + ", type=" + type + ", extension="
				+ extension + "]";
	}
}
