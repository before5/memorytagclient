package com.before5.domain;

import java.util.Date;

// 문의사항의 댓글을 관리하는 페이지 
public class QReplyVO {
	
	private Integer qno; // 어떤 문의사항에 댓글을 달았는지
	private Integer rno; // 댓글의 번호 primary key
	private String replyText; //댓글 내용
	private String replyer; //댓글을 단 사람
	private Date regdate; // 등록일 
	public Integer getQno() {
		return qno;
	}
	public void setQno(Integer qno) {
		this.qno = qno;
	}
	public Integer getRno() {
		return rno;
	}
	public void setRno(Integer rno) {
		this.rno = rno;
	}
	public String getReplyText() {
		return replyText;
	}
	public void setReplyText(String replyText) {
		this.replyText = replyText;
	}
	public String getReplyer() {
		return replyer;
	}
	public void setReplyer(String replyer) {
		this.replyer = replyer;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	@Override
	public String toString() {
		return "QReplyVO [qno=" + qno + ", rno=" + rno + ", replyText=" + replyText + ", replyer=" + replyer
				+ ", regdate=" + regdate + "]";
	}
	
	
	
}
