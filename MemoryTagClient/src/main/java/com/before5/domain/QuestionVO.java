package com.before5.domain;

import java.util.Date;

//공지사항 객체
public class QuestionVO {

	private Integer qno;
	private String title;
	private String content;
	private String id;
	private Date regdate;
	private Date updatedate;
	private int replycnt;
	private int family;
	private int depth;
	private int indent;
	private int parent;
		
	public int getFamily() {
		return family;
	}


	public void setFamily(int family) {
		this.family = family;
	}


	public int getDepth() {
		return depth;
	}


	public void setDepth(int depth) {
		this.depth = depth;
	}


	public int getIndent() {
		return indent;
	}


	public void setIndent(int indent) {
		this.indent = indent;
	}


	public int getParent() {
		return parent;
	}


	public void setParent(int parent) {
		this.parent = parent;
	}


	public int getReplycnt() {
		return replycnt;
	}


	public void setReplycnt(int replycnt) {
		this.replycnt = replycnt;
	}


	public QuestionVO() {
		super();
	}
	
	
	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public Integer getQno() {
		return qno;
	}
	public void setQno(Integer qno) {
		this.qno = qno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}


	@Override
	public String toString() {
		return "QuestionVO [qno=" + qno + ", title=" + title + ", content=" + content + ", id=" + id + ", regdate="
				+ regdate + ", updatedate=" + updatedate + ", replycnt=" + replycnt + ", family=" + family + ", depth="
				+ depth + ", indent=" + indent + ", parent=" + parent + "]";
	}
		
}
