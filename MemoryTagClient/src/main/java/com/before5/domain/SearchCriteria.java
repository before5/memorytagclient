package com.before5.domain;


public class SearchCriteria extends Criteria {  //Critetria 클래스를 상속함.
	
	//두 개의 instance Variable 추가 (page, perPageNum, searchType, keyword 4개 데이터가 전달됨)
	private String searchType;  //검색종류 searchType
	private String keyword;     //검색 키워드 keyword

	
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	public String getKeyword() {
		return keyword;
	}
	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}
	@Override
	public String toString() {
		return super.toString() + "SearchCriteria [searchType=" + searchType + ", keyword=" + keyword + "]";
	}
}	
