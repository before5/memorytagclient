package com.before5.domain;

public class LoginVO {			//로그인하기위해서 VO를 만듭니다.

	
	private String id;			//id
	private String pw;			//pw
	private boolean useCookie;	//Cookie체크를 하기위해서 만들어 뒀다.
	private int grade;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	@Override
	public String toString() {
		return "LoginVO [id=" + id + ", pw=" + pw + ", useCookie=" + useCookie + ", grade=" + grade + "]";
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public boolean isUseCookie() {
		return useCookie;
	}
	public void setUseCookie(boolean useCookie) {
		this.useCookie = useCookie;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
}