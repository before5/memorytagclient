package com.before5.domain;

import java.sql.Date;

// ORDER VO 작성
// orderdate, regdate, ono(주문번호), id, dno(디바이스번호)
// 작성자: 윤영

public class OrderVO {
	
	private Date orderDate;
	private Date regDate;
	private Integer ono;
	private String id;
	private int dno;
	
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public Date getRegDate() {
		return regDate;
	}
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	public Integer getOno() {
		return ono;
	}
	public void setOno(Integer ono) {
		this.ono = ono;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getDno() {
		return dno;
	}
	public void setDno(int dno) {
		this.dno = dno;
	}
	
	@Override
	public String toString() {
		return "OrderVO [orderDate=" + orderDate + ", regDate=" + regDate + ", ono=" + ono + ", id=" + id + ", dno="
				+ dno + "]";
	}
	
	
	
	

}
