package com.before5.domain;

import java.util.Date;

public class NReplyVO {

	private Integer nno;
	private Integer nrno;
	private String replytext;
	private String id;
	private Date regdate;
	private Date updatedate;
	public Integer getNno() {
		return nno;
	}
	public void setNno(Integer nno) {
		this.nno = nno;
	}
	public Integer getNrno() {
		return nrno;
	}
	public void setNrno(Integer nrno) {
		this.nrno = nrno;
	}
	public String getReplytext() {
		return replytext;
	}
	public void setReplytext(String replytext) {
		this.replytext = replytext;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getUpdatedate() {
		return updatedate;
	}
	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}
	@Override
	public String toString() {
		return "NReplyVO [nno=" + nno + ", nrno=" + nrno + ", replytext=" + replytext + ", id=" + id + ", regdate="
				+ regdate + ", updatedate=" + updatedate + "]";
	}
	
	
}
