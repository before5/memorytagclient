package com.before5.domain;

import java.util.Date;

// 노드한테 전해줄 vo
public class NodeVO {

	private Integer cno;
	private String title;
	private Date regdate;
	private Date orderdate;
	private String text;
	private Integer dno;
	private String fileName;
	private String type;
	private String directory;
	private Integer f_size;
	private String pw;
	private String fontcolor;
	private int fontsize;
	
	
	
	
	public String getFontcolor() {
		return fontcolor;
	}
	public void setFontcolor(String fontcolor) {
		this.fontcolor = fontcolor;
	}
	public int getFontsize() {
		return fontsize;
	}
	public void setFontsize(int fontsize) {
		this.fontsize = fontsize;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}
	public Integer getF_size() {
		return f_size;
	}
	public void setF_size(Integer f_size) {
		this.f_size = f_size;
	}
	public String getDirectory() {
		return directory;
	}
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	
	public Integer getCno() {
		return cno;
	}
	public void setCno(Integer cno) {
		this.cno = cno;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Date getRegdate() {
		return regdate;
	}
	public void setRegdate(Date regdate) {
		this.regdate = regdate;
	}
	public Date getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(Date orderdate) {
		this.orderdate = orderdate;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Integer getDno() {
		return dno;
	}
	public void setDno(Integer dno) {
		this.dno = dno;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public String toString() {
		return "NodeVO [cno=" + cno + ", title=" + title + ", regdate=" + regdate + ", orderdate=" + orderdate
				+ ", text=" + text + ", dno=" + dno + ", fileName=" + fileName + ", type=" + type + ", directory="
				+ directory + ", f_size=" + f_size + ", pw=" + pw + ", fontcolor=" + fontcolor + ", fontsize="
				+ fontsize + "]";
	}
	
	
}
