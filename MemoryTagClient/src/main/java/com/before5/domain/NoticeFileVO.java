package com.before5.domain;

import java.util.Arrays;
import java.util.Date;

public class NoticeFileVO {

	private Integer nfno;
	private Integer nno;
	private String original_file_name;
	private String extension;
	private String filedirectory;
	private Date uploaddate;
	private String stored_file_name;
	private long file_size;

	private String[] files;
	
	public String[] getFiles() {
		return files;
	}
	public void setFiles(String[] files) {
		this.files = files;
	}
	public Integer getNfno() {
		return nfno;
	}
	public void setNfno(Integer nfno) {
		this.nfno = nfno;
	}
	public Integer getNno() {
		return nno;
	}
	public void setNno(Integer nno) {
		this.nno = nno;
	}
	public String getOriginal_file_name() {
		return original_file_name;
	}
	public void setOriginal_file_name(String original_file_name) {
		this.original_file_name = original_file_name;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getFiledirectory() {
		return filedirectory;
	}
	public void setFiledirectory(String filedirectory) {
		this.filedirectory = filedirectory;
	}
	public Date getUploaddate() {
		return uploaddate;
	}
	public void setUploaddate(Date uploaddate) {
		this.uploaddate = uploaddate;
	}
	public String getStored_file_name() {
		return stored_file_name;
	}
	public void setStored_file_name(String stored_file_name) {
		this.stored_file_name = stored_file_name;
	}
	public long getFile_size() {
		return file_size;
	}
	public void setFile_size(long file_size) {
		this.file_size = file_size;
	}

	@Override
	public String toString() {
		return "NoticeFileVO [nfno=" + nfno + ", nno=" + nno + ", original_file_name=" + original_file_name
				+ ", extension=" + extension + ", filedirectory=" + filedirectory + ", uploaddate=" + uploaddate
				+ ", stored_file_name=" + stored_file_name + ", file_size=" + file_size + ", files=" + Arrays.toString(files) + "]";
	}
	
	
}
