package com.before5.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
//다른 아이피에서 접근 할수 있도록 Access접근을 허가하는 필터입니다.
/**
 * Servlet Filter implementation class CORSFilter
 */
public class CORSFilter implements Filter {

    /**
     * Default constructor. 
     */
    public CORSFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse)response;
		
		res.addHeader("Access-Control-Allow-Origin", "*");
		  res.setHeader("Access-Control-Allow-Methods", "POST, GET,PUT, DELETE"); //4가지 메소드를 받아 들인다는 뜻인거 같습니다.
		  res.setHeader("Access-Control-Max-Age", "3600");						//3600개를 최대로 잡는 거겠
		     res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
		  chain.doFilter(request, response);

		
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
