package com.before5.persistence;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.MemberVO;
@Repository
public class MemberInfoDAOImpl implements MemberInfoDAO {

	@Inject
	private SqlSession session;
	
	private static final String namespace = "com.before5.mapper.memberInfoMapper";
	
	@Override
	public MemberVO selectMemberInfo(String id) {
		// TODO Auto-generated method stub
		return session.selectOne(namespace+".selectMemberInfo", id);
	}

	@Override
	public void updateMemberInfo(MemberVO vo) throws Exception {
		// TODO Auto-generated method stub
		session.update(namespace+".updateMemberInfo", vo);
	}

	
	

	

}
