package com.before5.persistence;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.NReplyVO;

public interface NReplyDAO {

	public List<NReplyVO> listPage(Integer nno, Criteria cri) throws Exception;
	public void create(NReplyVO nvo) throws Exception;
	public void update(NReplyVO nvo) throws Exception;
	public void delete(Integer nrno) throws Exception;
	public int count(Integer nno) throws Exception;
}
