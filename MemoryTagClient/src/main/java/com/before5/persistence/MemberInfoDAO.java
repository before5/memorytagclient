package com.before5.persistence;

import java.util.List;

import com.before5.domain.MemberVO;

public interface MemberInfoDAO {

	public MemberVO selectMemberInfo(String id);
	
	public void updateMemberInfo(MemberVO vo) throws Exception;
}
