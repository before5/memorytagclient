package com.before5.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.Criteria;
import com.before5.domain.QuestionVO;
import com.before5.domain.SearchCriteria;

@Repository
public class QuestionDAOImpl implements QuestionDAO {

	@Inject // sqlsession사용 하기 위해서 인젝트한다.
	private SqlSession session;
	
	// 모든 공지사항을 가져오는 method
	@Override
	public List<QuestionVO> readAllQuestion() throws Exception {
		return session.selectList("com.before5.mapper.QuestionMapper.selectQuestion");
	}
	
	//qno로 하나의 공지사항에 대해 조회하는 method
	@Override
	public QuestionVO readOneQuestion(Integer qno) throws Exception{
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.QuestionMapper.selectOne",qno);
	}

	//qno로 공지사항을 삭제하는 method
	@Override
	public void deleteQuestion(Integer qno) throws Exception {
		// TODO Auto-generated method stub
		session.delete("com.before5.mapper.QuestionMapper.delete",qno);
	}

	@Override
	public int totalCount() throws Exception{
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.QuestionMapper.countTotal");
	}

	@Override
	public List<QuestionVO> pageList(Criteria cri) throws Exception{
		// TODO Auto-generated method stub
		return session.selectList("com.before5.mapper.QuestionMapper.selectPageList",cri);
	}

	@Override
	public void insertQuestion(QuestionVO vo) throws Exception {
		// TODO Auto-generated method stub
		session.insert("com.before5.mapper.QuestionMapper.insertQuestion",vo);
	}

	@Override
	public void updateQuestion(QuestionVO vo) throws Exception {
		// TODO Auto-generated method stub
		session.update("com.before5.mapper.QuestionMapper.updateQuestion",vo);
	}

	@Override
	public void deleteReply(int qno) throws Exception {
		// TODO Auto-generated method stub
		session.delete("com.before5.mapper.QuestionMapper.deleteQno",qno);
	}

	@Override
	public List<QuestionVO> listSearch(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return session.selectList("com.before5.mapper.QuestionMapper.listSearch",cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.QuestionMapper.listSearchCount",cri);
	}

	@Override
	public void updateReplyCnt(int qno, int amount) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> paramMap = new HashMap<String,Object>();
		
		paramMap.put("qno", qno);
		paramMap.put("amount", amount);
		
		session.update("com.before5.mapper.QuestionMapper.updateReplyCnt",paramMap);
	}

	@Override
	public void insertRply(QuestionVO vo) throws Exception {
		// TODO Auto-generated method stub
		session.insert("com.before5.mapper.QuestionMapper.insertReply",vo);
	}

	@Override
	public List<QuestionVO> listSearchReply(SearchCriteria cri) throws Exception {
		// TODO Auto-generated method stub
		return session.selectList("com.before5.mapper.QuestionMapper.replyList",cri);
	}

	@Override
	public int checkParent(int qno) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.QuestionMapper.checkParent",qno);
	}


}
