package com.before5.persistence;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.QuestionVO;
import com.before5.domain.SearchCriteria;

public interface QuestionDAO {

	public List<QuestionVO> readAllQuestion() throws Exception; // 모든 공지사항의 list를 가져오는 method
	
	public QuestionVO readOneQuestion(Integer qno) throws Exception; //하나만 클릭했을 때 하나의 공지사항을 가져오는 method 
	
	public void deleteQuestion(Integer qno) throws Exception; // 공지사항을 삭제하는 method
	
	public int totalCount() throws Exception; // 모든 데이터의 수를 계산하는 메소드 
	
	public List<QuestionVO> pageList(Criteria cri) throws Exception; // page에 맞는 데이터 list를 가져오는 메소드 
	
	public void insertQuestion(QuestionVO vo) throws Exception; // insert한다. 
	
	public void updateQuestion(QuestionVO vo) throws Exception;// update
	
	public void deleteReply(int qno) throws Exception;
	
	public List<QuestionVO> listSearch(SearchCriteria cri) throws Exception;// 검색 조건에 맞는 리스트를 가지고 옴
	
	public int listSearchCount(SearchCriteria cri) throws Exception; //검색결과의 데이터 숫자 
	
	public void updateReplyCnt(int qno, int amount) throws Exception;/// 댓글 숫자세기
	
	public void insertRply(QuestionVO vo) throws Exception;// 답글 등록하기
	
	public List<QuestionVO> listSearchReply(SearchCriteria cri) throws Exception; // 계층형 게시판 
	
	public int checkParent(int qno) throws Exception; //자식이 있는지 판별하는 코드
	
}
