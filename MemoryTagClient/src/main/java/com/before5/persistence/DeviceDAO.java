package com.before5.persistence;

import java.util.List;

import com.before5.domain.DeviceVO;

public interface DeviceDAO {
	
	public List<DeviceVO> list();

}
