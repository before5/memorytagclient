package com.before5.persistence;

import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.DeviceVO;

@Repository
public class DeviceDAOImpl implements DeviceDAO {
	
	@Inject
	private SqlSession session;
	
	private static String namespace = "com.before5.mapper.deviceMapper";

	@Override
	public List<DeviceVO> list() {
		
		return session.selectList(namespace + ".list");
	}
	
	

}
