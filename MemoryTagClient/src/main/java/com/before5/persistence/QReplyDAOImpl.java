package com.before5.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.Criteria;
import com.before5.domain.QReplyVO;

@Repository
public class QReplyDAOImpl implements QReplyDAO {

	@Inject // sqlsession사용 하기 위해서 인젝트한다.
	private SqlSession session;

	@Override
	public List<QReplyVO> allReply(int qno) throws Exception {
		// TODO Auto-generated method stub
		return session.selectList("com.before5.mapper.QReplyMapper.allreply", qno);
	}

	@Override
	public void addReply(QReplyVO vo)throws Exception {
		// TODO Auto-generated method stub
		session.insert("com.before5.mapper.QReplyMapper.addreply", vo);

	}

	@Override
	public void modifyReply(QReplyVO vo) throws Exception{
		// TODO Auto-generated method stub
		session.update("com.before5.mapper.QReplyMapper.modifyreply", vo);

	}

	@Override
	public void deleteReply(int rno) throws Exception{
		session.delete("com.before5.mapper.QReplyMapper.deletereply", rno);

	}

	@Override
	public List<QReplyVO> pageReply(Criteria cri, int qno) throws Exception{
		// TODO Auto-generated method stub

		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("qno", qno);
		paramMap.put("cri", cri); 
		//selectList 파라미터가 부족하니까 이렇게 map에 넣어서 보낸다

		return session.selectList("com.before5.mapper.QReplyMapper.pageReply",paramMap);
	}

	@Override
	public int countReply(int qno) throws Exception{
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.QReplyMapper.countreply", qno);
	}

	@Override
	public int getQno(int rno) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.QReplyMapper.getQno",rno);
	}

}
