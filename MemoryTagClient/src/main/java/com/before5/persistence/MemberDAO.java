package com.before5.persistence;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.MemberVO;
import com.before5.domain.SearchCriteria;

public interface MemberDAO {

	public void insertMember(MemberVO vo); // insertMember설정

	public MemberVO readMember(String id); // selectMember 설정(한개 조회)

	public List<MemberVO> selectAll(); // selectAll 설정(전체조회)

	public void updateMember(MemberVO vo); // update 설정(수정)

	public void deleteMember(String id); // delete 설정(삭제)
	
	public List<MemberVO> listPage(int page)throws Exception;
	
	public List<MemberVO> listCriteria(Criteria cri)throws Exception;
	
	public int countPaging(Criteria cri)throws Exception;
	
	public List<MemberVO> listSearch(SearchCriteria cri)throws Exception;
	
	public int listSearchCount(SearchCriteria cri)throws Exception;
	
}
