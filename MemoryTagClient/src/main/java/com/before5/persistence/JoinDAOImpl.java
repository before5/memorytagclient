package com.before5.persistence;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.MemberVO;

@Repository
public class JoinDAOImpl implements JoinDAO {

	@Inject // sqlsession사용 하기 위해서 인젝트한다.
	private SqlSession session;
	
	@Override
	public void joinMember(MemberVO vo) throws Exception {
		session.insert("com.before5.mapper.MemberMapper.insertMember",vo);
	}

	@Override
	public MemberVO searchId(String id) throws Exception {
		return session.selectOne("com.before5.mapper.MemberMapper.searchId",id);
	}


}
