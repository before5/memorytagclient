package com.before5.persistence;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.FileVO;
import com.before5.domain.MyPageVO;


public interface MyPageDAO {

	// 내 주문 내역을 가져오는 함수 
	public List<MyPageVO> myOrder(String id) throws Exception;
	
	//page별로 리스트 가져오는 함수 
	public List<MyPageVO> myListOrder(String id,Criteria cri) throws Exception;
	
	//카운트 하는 함수
	public int countOrder(String id) throws Exception;
	
	//하나만 읽어오는 함수 
	public MyPageVO readOrder(String id,int ono) throws Exception;
	
	//파일 번호로 파일 가져오는 함수
	public FileVO readFile(int fno) throws Exception;
	
	//tbl_content 수정
	public void updateContent(MyPageVO vo) throws Exception;
	
	//tbl_content 하나 조회
	public MyPageVO oneContent(int cno) throws Exception;
	
	//tbl_file update 
	public void updateFile(FileVO vo) throws Exception;
	
	//삭제
	public void deleteOrder(int ono)throws Exception;
	
	//ono로 cno 조회
	public int getCno(int ono) throws Exception;
	
	//cno 삭제
	public void deleteCno(int cno) throws Exception;
	
	//cno로 fno가져오기
	public FileVO getFile(int cno) throws Exception;
	
	//fno삭제
	public void deleteFno(int cno) throws Exception;
	
	//cno로 order조회
	public int getOrder(int cno) throws Exception;
}
