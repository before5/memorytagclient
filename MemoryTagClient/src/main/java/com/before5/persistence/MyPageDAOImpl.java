package com.before5.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.Criteria;
import com.before5.domain.FileVO;
import com.before5.domain.MyPageVO;


@Repository
public class MyPageDAOImpl implements MyPageDAO {

	@Inject
	private SqlSession session;

	@Override
	public List<MyPageVO> myOrder(String id) throws Exception {

		return session.selectList("com.before5.mapper.MypageMapper.myOrder", id);
	}

	@Override
	public List<MyPageVO> myListOrder(String id, Criteria cri) throws Exception {

		Map<String, Object> paramMap = new HashMap<>();

		paramMap.put("id", id);
		paramMap.put("cri", cri);
		return session.selectList("com.before5.mapper.MypageMapper.myOrderList", paramMap);
	}

	@Override
	public int countOrder(String id) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.MypageMapper.countOrder", id);
	}

	@Override
	public MyPageVO readOrder(String id, int ono) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> paramMap = new HashMap<>();

		paramMap.put("id", id);
		paramMap.put("ono", ono);
		return session.selectOne("com.before5.mapper.MypageMapper.readOne", paramMap);
	}

	@Override
	public FileVO readFile(int fno) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.MypageMapper.file", fno);
	}

	@Override
	public MyPageVO oneContent(int cno) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.MypageMapper.oneContent", cno);
	}

	@Override
	public void updateContent(MyPageVO vo) throws Exception {
		session.update("com.before5.mapper.MypageMapper.updateContent", vo);

	}

	@Override
	public void updateFile(FileVO vo) throws Exception {
		// TODO Auto-generated method stub
		session.update("com.before5.mapper.MypageMapper.updateFile",vo);
	}

	@Override
	public void deleteOrder(int ono) throws Exception {
		// TODO Auto-generated method stub
		session.delete("com.before5.mapper.MypageMapper.deleteOrder",ono);
	}

	@Override
	public int getCno(int ono) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.MypageMapper.getCno",ono);
	}

	@Override
	public void deleteCno(int cno) throws Exception {
		// TODO Auto-generated method stub
		session.delete("com.before5.mapper.MypageMapper.deleteCno",cno);
	}

	@Override
	public FileVO getFile(int cno) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.MypageMapper.getFile",cno);
	}

	@Override
	public void deleteFno(int cno) throws Exception {
		// TODO Auto-generated method stub
		session.delete("com.before5.mapper.MypageMapper.deleteFno",cno);
	}

	@Override
	public int getOrder(int cno) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne("com.before5.mapper.MypageMapper.readOrder",cno);
	}

}
