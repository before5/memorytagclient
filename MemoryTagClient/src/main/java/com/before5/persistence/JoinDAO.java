package com.before5.persistence;

import java.util.List;

import com.before5.domain.MemberVO;

public interface JoinDAO {

	public void joinMember(MemberVO vo) throws Exception;
	// 회원등록
	
	public MemberVO searchId(String id) throws Exception;
}
