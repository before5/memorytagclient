package com.before5.persistence;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.Criteria;
import com.before5.domain.NReplyVO;
@Repository
public class NReplyDAOImpl implements NReplyDAO {

	@Inject
	private SqlSession session;
	
	private static final String namespace = "com.before5.mapper.nreplyMapper";
	
	@Override
	public List<NReplyVO> listPage(Integer nno, Criteria cri) throws Exception {
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("nno", nno);
		paramMap.put("cri", cri);
		return session.selectList(namespace+".listPage", paramMap);
	}

	@Override
	public void create(NReplyVO nvo) throws Exception {
		session.insert(namespace+".create", nvo);
	}

	@Override
	public void update(NReplyVO nvo) throws Exception {
		session.update(namespace+".update", nvo);

	}

	@Override
	public void delete(Integer nrno) throws Exception {
		// TODO Auto-generated method stub
		session.delete(namespace+".delete", nrno);
	}

	@Override
	public int count(Integer nno) throws Exception {
		// TODO Auto-generated method stub
		return session.selectOne(namespace+".count", nno);
	}

}
