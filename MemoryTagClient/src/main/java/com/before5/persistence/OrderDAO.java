package com.before5.persistence;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.OrderVO;

import com.before5.domain.SearchCriteria;

public interface OrderDAO {
	
	public void delete(Integer ono) throws Exception;  //삭제 

	public List<OrderVO> listAll() throws Exception;  //전체 목록 조회
	
	public List<OrderVO> listPage(int page) throws Exception;  
	
	public List<OrderVO> listCriteria(Criteria cri) throws Exception;  //페이지 처리된 전체 목록 조회
	
	public int countPaging(Criteria cri) throws Exception; //실제 게시물 숫자를 이용한 페이징 처리
	
	public List<OrderVO> listSearch(SearchCriteria cri) throws Exception; // 검색용 리스트
	 
	public int listSearchCount(SearchCriteria cri) throws Exception; //검색의 페이징


}
