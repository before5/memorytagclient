package com.before5.persistence;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.LoginVO;
@Repository				//DAO에 거는 어노테이션 @Repository		
public class LoginDAOImpl implements LoginDAO {				//LoginDAO를 상속받아서 메소드들을 만든다.

	@Inject
	private SqlSession session;			//session를 주입한다.(DB와 데이터를 주고 받기 위하여)
	
	private static String namespace = "com.before5.mapper.loginMapper";	//편할려고 네임스페이스 만듬
	
	
	@Override
	public LoginVO login(LoginVO vo) throws Exception {	//로그인 메소드를 만든다.
	
		return session.selectOne(namespace+".login",vo);	//세션에서 한개만 데이터를 주고받기 때문에 selectOne기능을 쓰고 매퍼를 vo를 가지고 실행시킨다.
	}


	@Override
	public void keepLogin(String id, String sessionId, Date next) {//세션을 만드는 메소드

		
		Map<String, Object> paraMap = new HashMap<String,Object>();//맵을 만들어서 데이터들을 집어넣고
		paraMap.put("id", id);										//id를 집어넣고
		paraMap.put("sessionId", sessionId);						//sessinKey값을 집어넣습니다.
		paraMap.put("next", next);									//내일(?) 값을 집어넣습니다.

		session.update(namespace+".keepLogin",paraMap);				//업데이트 매퍼를 동작시켜서 paraMap으로 받은 데이터들을 업데이트해줍니다.
	}


	@Override
	public LoginVO checkLoginWithSessionKey(String value) {
		
		return session.selectOne(namespace + ".checkLoginWithSessionKey", value);	//기간이 지금보다 더 미래라면 데이터를 가져옵니다. 따라서 자동로그인이 됩니다.
	}

}
