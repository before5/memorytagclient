package com.before5.persistence;

import java.util.List;

import com.before5.domain.Criteria;
import com.before5.domain.NoticeFileVO;
import com.before5.domain.NoticeVO;
import com.before5.domain.SearchCriteria;

public interface NoticeDAO {

	public void create(NoticeVO vo) throws Exception;
	public NoticeVO read(Integer nno) throws Exception;
	public void update(NoticeVO vo) throws Exception;
	public void delete(Integer nno) throws Exception;
	//public List<NoticeVO> listAll() throws Exception;
	//public List<NoticeVO> listPage(int page) throws Exception;
	public List<NoticeVO> listCriteria(Criteria cri) throws Exception;
	public int countPaging(Criteria cri) throws Exception;
	public List<NoticeVO> listSearch(SearchCriteria cri) throws Exception;
	public int listSearchCount(SearchCriteria cri) throws Exception;
	public void addAttach(NoticeFileVO filevo) throws Exception;
	public void addAttach2(NoticeFileVO filevo) throws Exception;
	//public void updateAttach(NoticeFileVO filevo) throws Exception;
	public List<String> getAttach(Integer nno) throws Exception;
	public void updateViewCnt(Integer nno);
}
