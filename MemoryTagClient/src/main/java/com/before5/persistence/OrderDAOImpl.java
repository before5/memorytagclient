package com.before5.persistence;

import java.sql.Date;
import java.util.List;

import javax.inject.Inject;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import com.before5.domain.Criteria;
import com.before5.domain.OrderVO;

import com.before5.domain.SearchCriteria;

@Repository
public class OrderDAOImpl implements OrderDAO {
	
	@Inject
	private SqlSession session;
	
	private static String namespace = "com.before5.mapper.orderMapper";

	@Override
	public List<OrderVO> listAll() throws Exception {
		
		return session.selectList(namespace + ".listAll");
	}

	@Override
	public void delete(Integer ono) throws Exception {
		
		session.delete(namespace+".delete", ono);
		
	}

	@Override
	public List<OrderVO> listPage(int page) throws Exception {

		if (page <= 0) {
			page = 1;
		}
		page = (page - 1) * 10;

		return session.selectList(namespace + ".listPage" + page);
	}	

	@Override
	public List<OrderVO> listCriteria(Criteria cri) throws Exception {
		
		return session.selectList(namespace + ".listCri", cri);
	}

	@Override
	public int countPaging(Criteria cri) throws Exception {
		
		return session.selectOne(namespace + ".countPaging", cri);
	}

	@Override
	public List<OrderVO> listSearch(SearchCriteria cri) throws Exception {
		
		return session.selectList(namespace + ".listSearch", cri);
	}

	@Override
	public int listSearchCount(SearchCriteria cri) throws Exception {
		
		return session.selectOne(namespace + ".listSearchCount", cri);
	}


}
