package com.before5.persistence;

import static org.junit.Assert.fail;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class Question {

	@Inject 
	QReplyDAO dao;
	@Test
	public void test() throws Exception{
		int qno = dao.getQno(163);
		System.out.println(qno);
	}

}
