package com.before5.persistence;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.domain.MemberVO;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class MemberInfoDAOTest {

	@Inject
	private MemberInfoDAO dao;
	
	@Test
	public void testSelectMemberInfo() {
	
	
		System.out.println(dao.selectMemberInfo("young49137313665184"));
	}

	@Test
	public void testupdateMemberInfo() throws Exception {
	
        MemberVO vo = new MemberVO();
        vo.setId("young49137313665184");
        vo.setEmail("young@hanmail.net");
        vo.setGender("M");
        vo.setPhone("01011111111");
        vo.setPw("young1111");
        vo.setName("윤영");
		dao.updateMemberInfo(vo);
	}
}
