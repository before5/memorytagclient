package com.before5.persistence;

import java.util.List;

import javax.inject.Inject;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.before5.domain.Criteria;
import com.before5.domain.NReplyVO;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "file:src/main/webapp/WEB-INF/spring/**/*.xml" })
public class NReplyDAOTest {

	@Inject
	private NReplyDAO dao;
	
//	@Test
//	public void testList() throws Exception { 
//	List<NReplyVO> list = dao.listPage(67);
//	for(int i=0; i<list.size();i++){
//		System.out.println(list.get(i)); //커밋 꼭 하기 커밋 안 하면 리스트가 안 나온다.
//	}
//	}


	@Test
	public void testList() throws Exception { 
		Criteria cri = new Criteria();
		cri.setPage(1);
		cri.setPerPageNum(10);
	List<NReplyVO> list = dao.listPage(67, cri);
	for(int i=0; i<list.size();i++){
		System.out.println(list.get(i)); //커밋 꼭 하기 커밋 안 하면 리스트가 안 나온다.
	}
	}
	
	@Test
	public void testUpdate() throws Exception { 
	NReplyVO vo = new NReplyVO();
	vo.setNrno(2);
	vo.setReplytext("리플테스트");
	dao.update(vo);
	}
	
	@Test
	public void testCreate() throws Exception { 
	NReplyVO vo = new NReplyVO();
	vo.setNno(67);
	vo.setId("abc");
	vo.setReplytext("테스트입니다.");
	
	dao.create(vo);
	}
	
	@Test
	public void testDelete() throws Exception { 
	
	dao.delete(3);
	}
}
